# Делаем проект многопоточным

## Цель

Применение разных способов распараллеливания задач и оценка оптимального способа реализации.

## Задача

1. Напишите вычисление суммы элементов массива интов: 
    - а. Обычное 
    - б. Параллельное (для реализации использовать Thread, например List) 
    - в. Параллельное с помощью LINQ

2. Замерьте время выполнения для 100 000, 1 000 000 и 10 000 000 элементов, укажите:

    - Окружение
    - Код последовательного вычисления и время его выполнения
    - Код параллельного вычисления и время его выполнения
    - Код LINQ и время его выполнения

3. Добавьте результаты для всех 3 замеров в таблицу: https://docs.google.com/spreadsheets/d/1WQwijkBc_BGmKRikTBoO7hLw8EX4yHgQyI_0aTLgB7c/edit?usp=sharing

Пришлите в чат с преподавателем помимо ссылки на репозиторий номера своих строк в таблице.

## Алгоритмы

Для последовательного расчета суммы использовался следующий код

```c#
public int Count(IEnumerable<int> items) => items.Sum();
```

Для расчета с помощью потоков 

```c#
 public int Count(IEnumerable<int> items)
 { 
    int ch = 0;
    var chuncks = items.GroupBy(x => ++ch % ThreadsCount).ToList();
    var bag = new ConcurrentBag<int>();

    List<Thread> threads = new List<Thread>(ThreadsCount);
    for (int i = 0; i < chuncks.Count; i++)
    {
        int s = i;
        var thread = new Thread(() => bag.Add(chuncks.ElementAt(s).Sum()));
        threads.Add(thread);
        thread.Start();
    }
    threads.ForEach(s => s.Join());
    return bag.Sum();
}
```
При использовании PLINQ

```c#
  public int Count(IEnumerable<int> items) => items.AsParallel().Sum();
```

## Результаты

## SYSTEMINFO

|  Параметр  | Значение |
|--------|-------|
| Название ОС | Майкрософт Windows 10 Pro |
| Версия ОС | 10.0.19043 Н/Д построение 19043 |
| Изготовитель ОС | Microsoft Corporation |
| Сборка ОС | Multiprocessor Free | 
| Изготовитель системы | ASUSTeK Computer Inc. |
| Модель системы | K53SJ |
| Тип системы | x64-based PC |
| Процессор(ы) | Intel64 Family 6 Model 42 Stepping 7 GenuineIntel ~2001 МГц |
   ## Таблица результатов работы

| Кол-во элементов  | Consistently | Parallel (4 threads) | Parallel (8 threads)| PLINQ |
|-------------------|--------------|----------------------|---------------------|-------|
| 100 000           |   3,4394 |  90,1619 | 210,552  | 102,5535 |
| 1 000 000         |  22,9003 | 178,4482 | 212,6359 |  45,3698 |
| 10 000 000        | 237,0962 | 630,1333 | 704,7607 | 188,0268 |

*Показатели времени в милисекундах*