﻿using System.Collections.Generic;
using System.Linq;

namespace Parallel.Import.Domain.Strategies
{
    public class PLINQCounterStrategy : ICounterStrategy
    {
        public int Count(IEnumerable<int> items)
            => items.AsParallel().Sum();
    }
}
