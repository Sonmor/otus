﻿using System.Collections.Generic;
using System.Linq;

namespace Parallel.Import.Domain.Strategies
{
    public class CommonCounterStrategy : ICounterStrategy
    {
        public int Count(IEnumerable<int> items)
            => items.Sum();
    }
}
