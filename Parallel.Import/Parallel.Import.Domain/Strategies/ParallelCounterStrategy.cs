﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Parallel.Import.Domain.Strategies
{
    public class ParallelCounterStrategy : ICounterStrategy
    {
        private readonly int ThreadsCount;
        public ParallelCounterStrategy(int ThreadsCount)
        {
            this.ThreadsCount = ThreadsCount;
        }
        public int Count(IEnumerable<int> items)
        {
            int ch = 0;
            var chuncks = items.GroupBy(x => ++ch % ThreadsCount).ToList();
            var bag = new ConcurrentBag<int>();

            List<Thread> threads = new List<Thread>(ThreadsCount);
            for (int i = 0; i < chuncks.Count; i++)
            {
                int s = i;
                var thread = new Thread(() => bag.Add(chuncks.ElementAt(s).Sum()));
                threads.Add(thread);
                thread.Start();
            }
            threads.ForEach(s => s.Join());
            return bag.Sum();
        }
    }
}
