﻿using Parallel.Import.Domain.Inf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Parallel.Import.Domain
{
    public class Counter
    {
        private readonly ICounterStrategy strategy;
        private readonly IParallelApp app;
        public Counter(ICounterStrategy strategy, IParallelApp app)
        {
            this.strategy = strategy;
            this.app = app;
        }

        public Report Sum(IEnumerable<int> items)
        {
            var watch = new Stopwatch();
            int sum = -1;
            bool flag = false;
            int count = 0;
            TimeSpan time = TimeSpan.Zero;
            try
            {
                if (items == null) throw new ArgumentNullException(nameof(items));
                watch.Start();
                sum = strategy.Count(items);
                watch.Stop();
                flag = true;
                count = items.Count();
                time = watch.Elapsed;
            }
            catch (Exception ex)
            {
                app.OnThrowError(ex.Message);
                watch.Stop();
            }

            return new Report
            {
                IsReportCorrect = flag,
                CounterType = strategy.GetType().Name,
                ItemsCount = count,
                Sum = sum,
                Time = time
            };
        }
    }
}
