﻿using System;

namespace Parallel.Import.Domain.Inf
{
    public interface IParallelApp
    {
        event Action Exit;
        void OnWrite(string message);
        void OnThrowError(string message);
        string OnRead();
        void OnExit();
        void OnClear();
        Report GetSum(ICounterStrategy strategy, int ItemsCount);
    }
}
