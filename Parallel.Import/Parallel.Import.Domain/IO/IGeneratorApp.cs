﻿using System;

namespace Parallel.Import.Domain.IO
{
    public interface IGeneratorApp
    {
        event Action Exit;
        string OutputFile { get; set; }
        int DataCount { get; set; }
        string OnRead();
        void OnWrite(string message);
        void OnThrowError(string error);
        void OnExit();
    }
}
