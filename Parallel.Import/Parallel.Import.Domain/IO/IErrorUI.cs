﻿namespace Parallel.Import.Domain.IO
{
    public interface IErrorUI
    {
        void ThrowError(string error);
    }
}
