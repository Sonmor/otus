﻿namespace Parallel.Import.Domain.IO
{
    public interface IReaderUI
    {
        string Read();
        void Clear();
    }
}
