﻿namespace Parallel.Import.Domain.IO
{
    public enum CounterStrategyType
    {
        Common,
        Parallel,
        PLINQ
    }
}
