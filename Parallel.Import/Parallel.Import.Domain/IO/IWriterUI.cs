﻿namespace Parallel.Import.Domain.IO
{
    public interface IWriterUI
    {
        void Write(string message);
    }
}
