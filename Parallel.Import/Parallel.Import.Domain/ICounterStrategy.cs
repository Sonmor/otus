﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parallel.Import.Domain
{
    public interface ICounterStrategy
    {
        int Count(IEnumerable<int> items);
    }
}
