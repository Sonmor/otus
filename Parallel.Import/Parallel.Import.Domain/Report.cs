﻿using System;

namespace Parallel.Import.Domain
{
    public class Report
    {
        public bool IsReportCorrect { get; set; }
        public int Sum { get; set; }
        public int ItemsCount { get; set; }
        public string CounterType { get; set; }
        public TimeSpan Time { get; set; }
    }
}