﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
namespace Parallel.Import.Domain.Strategies.Tests
{
    [TestFixture]
    public class CommonCounterStrategyTests
    {
        CommonCounterStrategy Create() => new CommonCounterStrategy();

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void Count_ReturnExpectedSum_True()
        {
            var items = new List<int> { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
            var sum = Create().Count(items);
            Assert.That(sum, Is.EqualTo(items.Sum()));
        }
        [Test]
        public void Count_SendEmptyCollection_SumZero()
        {
            var items = Enumerable.Empty<int>();
            var sum = Create().Count(items);
            Assert.That(sum, Is.EqualTo(0));
        }
    }
}
