using System.Linq;
using NUnit.Framework;
using Moq;
using Parallel.Import.Domain.Inf;
using System.Collections.Generic;
using System;

namespace Parallel.Import.Domain.Tests
{
    public class CounterTests
    {
        private Counter Create(ICounterStrategy strategy = null, IParallelApp app = null)
        {
            strategy ??= new Mock<ICounterStrategy>().Object;
            app ??= new Mock<IParallelApp>().Object;
            return new Counter(strategy: strategy, app: app);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void SumMethod_AreCallStrategyCountMethod_True()
        {
            var mock = new Mock<ICounterStrategy>();
            var counter = Create(strategy: mock.Object);
            _ = counter.Sum(Enumerable.Empty<int>());
            mock.Verify(x => x.Count(It.IsAny<IEnumerable<int>>()), Times.Once);
        }
        [Test]
        public void SumMethod_StrategyCountMethodThrowException_AppThrowError()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Throws<Exception>();
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            _ = counter.Sum(It.IsAny<IEnumerable<int>>());
            app.Verify(x => x.OnThrowError(It.IsAny<string>()), Times.Once); 
        }

        [Test]
        public void SumMethod_StrategyCountMethodThrowException_ReportHasStatusFalse()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Throws<Exception>();
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(It.IsAny<IEnumerable<int>>());
            Assert.That(report.IsReportCorrect, Is.False);
        }
        [Test]
        public void SumMethod_StrategyCountMethodFinishCorrently_ReportHasStatusTrue()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Returns(1000);
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(new List<int> {1,2,3,5,6 });
            Assert.That(report.IsReportCorrect, Is.True);
        }
        [Test]
        public void SumMethod_ReportSumExpected_True()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Returns(1000);
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(new List<int> {1,2,3,4,5 });
            Assert.That(report.Sum, Is.EqualTo(1000));
        }
        
        [Test]
        public void SumMethod_SendNullCollections_AppThrowError()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Returns(1000);
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(null);
            app.Verify(x => x.OnThrowError(It.IsAny<string>()), Times.Once); 
        }
        [Test]
        public void SumMethod_SendEmptyCollections_NeverAppThrowError()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Returns(1000);
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(Enumerable.Empty<int>());
            app.Verify(x => x.OnThrowError(It.IsAny<string>()), Times.Never);
        }
        [Test]
        public void SumMethod_SendCorrectData_ReportTimeIsNotNull()
        {
            var strategy = new Mock<ICounterStrategy>();
            strategy.Setup(x => x.Count(It.IsAny<IEnumerable<int>>())).Returns(1000);
            var app = new Mock<IParallelApp>();

            var counter = Create(strategy: strategy.Object, app: app.Object);
            var report = counter.Sum(new List<int> { 1, 2, 3, 4, 5 });
            Assert.That(report.Time, Is.Not.Null);
        }
    }
}