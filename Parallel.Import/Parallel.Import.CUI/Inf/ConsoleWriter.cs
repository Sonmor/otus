﻿using Parallel.Import.Domain.IO;
using System;
namespace Parallel.Import.CUI.Inf
{
    public class ConsoleWriter : IWriterUI
    {
        public void Write(string message)
            => Console.Write(message.Replace("\n", Environment.NewLine));
    }
}
