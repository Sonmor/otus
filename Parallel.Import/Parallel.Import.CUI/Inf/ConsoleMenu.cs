﻿using System;
using System.Collections.Generic;

namespace Parallel.Import.CUI.Inf
{
    public class ConsoleMenu
    {
        public List<Tuple<int, string, Action>> MenuItems = new();

        public object DisplayMenu()
        {
            Console.WriteLine("=======================================");
            Console.WriteLine("||    Please choose menun item        ||");
            Console.WriteLine("=======================================");

            foreach (var item in MenuItems)
            {
                Console.WriteLine($" {item.Item1} : {item.Item2}");
            }
            Console.WriteLine("=======================================");
            return 0;
        }

    }
}
