﻿using Parallel.Import.Domain.IO;
using System;
namespace Parallel.Import.CUI.Inf
{
    public class ConsoleReader : IReaderUI
    {
        public string Read() => Console.ReadLine();
        public void Clear() => Console.Clear();
    }
}
