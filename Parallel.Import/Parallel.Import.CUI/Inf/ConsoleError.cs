﻿using Parallel.Import.Domain.IO;
using System;
namespace Parallel.Import.CUI.Inf
{
    public class ConsoleError : IErrorUI
    {
        public void ThrowError(string error)
            => Console.WriteLine($"Error: {error.Replace("\n", Environment.NewLine)}");
    }
}
