﻿using Parallel.Import.Domain;
using Parallel.Import.Domain.Inf;
using Parallel.Import.Domain.IO;
using System;
using System.Linq;

namespace Parallel.Import.CUI
{
    public class ParallelApp : IParallelApp
    {
        public event Action Exit;
        private readonly IWriterUI writer;
        private readonly IReaderUI reader;
        private readonly IErrorUI error;

        public ParallelApp(IWriterUI writer, IReaderUI reader, IErrorUI error)
        {
            this.writer = writer;
            this.reader = reader;
            this.error = error;
        }

        public void OnExit() => Exit.Invoke();

        public string OnRead() => reader.Read();
        public void OnThrowError(string message) => error.ThrowError(message);
        public void OnWrite(string message) => writer.Write(message);
        public void OnClear() => reader.Clear();
        public Report GetSum(ICounterStrategy strategy, int ItemsCount) => (new Counter(strategy, this)).Sum(Enumerable.Repeat(1,ItemsCount));
    }
}
