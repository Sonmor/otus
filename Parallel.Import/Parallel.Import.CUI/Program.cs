﻿using Parallel.Import.CUI.Inf;
using Parallel.Import.Domain;
using Parallel.Import.Domain.Inf;
using Parallel.Import.Domain.Strategies;
using System;
using System.Linq;

namespace Parallel.Import.CUI 
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new(new ParallelApp(writer: new ConsoleWriter(), reader: new ConsoleReader(), error: new ConsoleError()));
            prog.OnStart();
        }
        private readonly IParallelApp app;
        private readonly ConsoleMenu menu;

        protected Program(IParallelApp app)
        {
            this.app = app;
            this.menu = new();
            menu.MenuItems.Add(new Tuple<int, string, Action>(0, "Exit", Finish));
            menu.MenuItems.Add(new Tuple<int, string, Action>(1, "Computate Consistently", OnConsistently));
            menu.MenuItems.Add(new Tuple<int, string, Action>(2, "Computate Parallel", OnParallel));
            menu.MenuItems.Add(new Tuple<int, string, Action>(3, "Computate PLINQ", OnPLINQ));

            app.Exit += Finish;
        }
        internal void OnParallel()
        {
            app.OnWrite("+----------------------------------+\n");
            app.OnWrite("| Use some Threads for compute sum |\n");
            app.OnWrite("+----------------------------------+\n");

            int count = GetCountOfElements("Please enter count of items");
            int threads = GetCountOfElements("Please enter number of threads");
            try
            {
                ICounterStrategy strategy = new ParallelCounterStrategy(threads);
                var report = app.GetSum(strategy, count);
                PrintReport(report);
            }
            catch (Exception ex)
            {
                app.OnThrowError(ex.Message);
            }
        }
        internal void OnPLINQ()
        {
            app.OnWrite("+----------------------------+\n");
            app.OnWrite("|  Use PLINQ for compute sum |\n");
            app.OnWrite("+----------------------------+\n");

            int count = GetCountOfElements("Please enter count of items");
            try
            {
                ICounterStrategy strategy = new PLINQCounterStrategy();
                var report = app.GetSum(strategy, count);
                PrintReport(report);

            }
            catch (Exception ex)
            {
                app.OnThrowError(ex.Message);
            }
        }
        internal void OnConsistently()
        {
            app.OnWrite("+-----------------------------------------+\n");
            app.OnWrite("|  Use consistently mentod to compute sum |\n");
            app.OnWrite("+-----------------------------------------+\n");
            int count = GetCountOfElements("Please enter count of items"); 

            try
            {
                ICounterStrategy strategy = new CommonCounterStrategy();
                var report = app.GetSum(strategy, count);
                PrintReport(report);
            }
            catch (Exception ex)
            {
                app.OnThrowError(ex.Message);
            }
        }

        private int GetCountOfElements(string message)
        {
            int count = -1;
            app.OnWrite($"{message} : ");
            while (!int.TryParse(app.OnRead(), out count))
            {
                app.OnThrowError("Incorrect value, please enter id again");
            }
            return count;
        }

        internal void PrintReport(Report report)
        {
            app.OnWrite( "=====================================================\n");
            app.OnWrite($" Is report status correct : {report.IsReportCorrect} \n");
            app.OnWrite($"             Counter type : {report.CounterType}\n");
            app.OnWrite($"        Count of elements : {report.ItemsCount}\n");
            app.OnWrite($"                      Sum : {report.Sum}\n");
            app.OnWrite($"                Time (mc) : {report.Time.TotalMilliseconds}\n");
            app.OnWrite( "=====================================================\n");
        }


        public void OnStart()
        {
            app.OnWrite("+-------------------------------------+\n");
            app.OnWrite("|    Welcome to Parallel apllication  |\n");
            app.OnWrite("+-------------------------------------+\n");
            app.OnWrite("Please press any key ... ");
            _ = app.OnRead();
            do
            {
                app.OnClear();
                _ = menu.DisplayMenu();
                if (!int.TryParse(app.OnRead(), out int num))
                {
                    app.OnThrowError("Incorrect value, please send again ..\n");
                    continue;
                }
                try
                {
                    menu.MenuItems.First(x => x.Item1 == num).Item3();
                }
                catch
                {
                    app.OnThrowError($"Our menu havn't numer {num}. Please send another number");
                }
                app.OnWrite("Please press enter to continue... ");
                _ = app.OnRead();

            } while (true);
        }

        internal void Finish()
        {
            app.OnWrite("+----------------------------------+\n");
            app.OnWrite("| Parallel application was closed  |\n");
            app.OnWrite("+----------------------------------+\n");
            app.OnWrite("Please press any key to exit ...");
            _ = app.OnRead();

            Environment.Exit(0);
        }
    }
}
