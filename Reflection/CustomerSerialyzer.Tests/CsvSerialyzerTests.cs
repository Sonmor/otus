using CustomSerialyzer.Lib.Csv;
using CustomSerialyzer.Lib.Classes;
using NUnit.Framework;

namespace CustomSerialyzer.Csv.Tests
{
    public class CsvSerialyzerTests
    {
        private CsvSerialyzer Create() => new CsvSerialyzer();

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.IsNotNull(Create());
        [Test]
        public void SerialyzerF_AreEqualOutputStringExpected_True()
        {
            F f = F.Get();
            var expected = "i1,i2,i3,i4,i5\r\n1,2,3,4,5\r\n";
            var serialyzer = Create();
            Assert.AreEqual(expected, serialyzer.Serialize<F>(f));
        }
        [Test]
        public void SerialyzeCustomClass1_AreEqualOutputStringExpected_True()
        {
            var custom1 = new CustomClass1() 
            {
                Prop1 = 10,
                Prop2 = "Value of Prop2",
                Prop3 = 45.4675f,
                Field1 = 2,
                Field2 = "Some field value",
                Field3 = 3.1416f
            };
            var expected = "Prop1,Prop2,Prop3,Field1,Field2,Field3\r\n10,Value of Prop2,45.4675,2,Some field value,3.1416\r\n";
            var serialuzer = Create();
            Assert.AreEqual(expected,serialuzer.Serialize<CustomClass1>(custom1));
        }
        [Test]
        public void DeserialyzerF_AreEqualOutputObjectExpected_True()
        {
            F expected = F.Get();
            var csv_string = "i1,i2,i3,i4,i5\r\n1,2,3,4,5\r\n";
            var actual = Create().Deserialize<F>(csv_string);
            Assert.Multiple(() => 
            {
                Assert.AreEqual(expected.i1, actual.i1);
                Assert.AreEqual(expected.i2,actual.i2);
                Assert.AreEqual(expected.i3,actual.i3);
                Assert.AreEqual(expected.i4,actual.i4);
                Assert.AreEqual(expected.i5,actual.i5);
            });
        }
        [Test]
        public void DeserialyzeCustomClass1_AreEqualOutputObjectExpected_True()
        {
            var expected = new CustomClass1()
            {
                Prop1 = 10,
                Prop2 = "Value of Prop2",
                Prop3 = 45.4675f,
                Field1 = 2,
                Field2 = "Some field value",
                Field3 = 3.1416f
            };
            var csv_string = "Prop1,Prop2,Prop3,Field1,Field2,Field3\r\n10,Value of Prop2,45.4675,2,Some field value,3.1416\r\n";
            var actual = Create().Deserialize<CustomClass1>(csv_string);
            Assert.Multiple(() => 
            {
                Assert.AreEqual(expected.Prop1, actual.Prop1);
                Assert.AreEqual(expected.Prop2, actual.Prop2);
                Assert.AreEqual(expected.Prop3, actual.Prop3);
                Assert.AreEqual(expected.Field1, actual.Field1);
                Assert.AreEqual(expected.Field2, actual.Field2);
                Assert.AreEqual(expected.Field3, actual.Field3);
            });
        }
    }
}