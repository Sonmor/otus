﻿namespace CustomSerialyzer.Lib
{
    public struct TestTime
    {
        public string ClassName { get; }
        public string SerialyzerName { get;}
        public StatusOperation Status { get;}
        public double Time { get;}
        public TestTime(string ClassName, string SerialyzerName, StatusOperation Status, double Time)
        {
            this.ClassName = ClassName;
            this.SerialyzerName = SerialyzerName;
            this.Status = Status;
            this.Time = Time;
        }
    }
}
