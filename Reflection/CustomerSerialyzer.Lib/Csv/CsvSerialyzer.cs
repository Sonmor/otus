﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Globalization;

namespace CustomSerialyzer.Lib.Csv
{
    public class CsvSerialyzer
    {
        public T Deserialize<T>(string stream) where T: new()
        {
            var lines = stream.Split(Environment.NewLine);
            if (!lines.Any() || lines.Length < 2) throw new ArgumentException(nameof(stream));

            IEnumerable<string> names = lines[0].Split(',');
            IEnumerable<string> values = lines[1].Split(',');
            
            if (!names.Any() || !values.Any() || names.Count() != values.Count()) throw new ArgumentException("Incorrect file data");
            var members = names.Zip(
                values,
                (n,v) => new { Name = n, Value = v}
                );
            var result = new T();
            var properties = typeof(T).GetProperties();
            var fields = typeof(T).GetFields();
            foreach (var m in members)
            {
                if(properties.Any(x => x.Name == m.Name))
                {
                    var p = properties.First(x => x.Name == m.Name);
                    var value = Convert.ChangeType(m.Value, p.PropertyType,CultureInfo.InvariantCulture);
                    p.SetValue(result, value);
                    
                }

                if (fields.Any(x => x.Name == m.Name))
                {
                    var f = fields.First(x => x.Name == m.Name);
                    var value = Convert.ChangeType(m.Value, f.FieldType, CultureInfo.InvariantCulture);
                    f.SetValue(result, value);
                }
                
            }
            return result;
        }

        public string Serialize<T>(T item)
        {
            StringBuilder builder = new StringBuilder();
            var properties = typeof(T).GetProperties().Select(x => new { Name = x.Name, Value = x.GetValue(item)}).ToList() ;
            var fields = typeof(T).GetFields().Select(x => new { Name = x.Name, Value = x.GetValue(item)}).ToList();
            properties.AddRange(fields);

            builder.AppendLine(String.Join(",",properties.Select(x => x.Name).ToArray()));
            builder.AppendLine(String.Join(",", properties.Select(x => x.Value.ToString().Replace(',','.')).ToArray()));

            return builder.ToString();
        }
    }
}
