﻿using System;

namespace CustomSerialyzer.Lib
{
    [Flags]
    public enum StatusOperation
    {
        Serialization = 1,
        Deserization = 2
    }
}
