﻿using System;
using CustomSerialyzer.Lib;
using CustomSerialyzer.Lib.Csv;
using CustomSerialyzer.Lib.Classes;
using System.Collections.Generic;
using System.Linq;

namespace CustomSerialyzer.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Custom  Serialyzer");

            List<TestTime> dates = new List<TestTime>();
            // Class F - customer serialyzer
            var f = F.Get();
            var serializer = new CsvSerialyzer();
            dates.Add(TestSerialyze<F>(f, typeof(CsvSerialyzer).FullName,serializer.Serialize,out string csv_string));
            dates.Add(TestDeserialyze<F>(csv_string,typeof(CsvSerialyzer).FullName,serializer.Deserialize<F>,out F f2));

            // Newtonsoft.Json;
            dates.Add(TestSerialyze<F>(f,typeof(Newtonsoft.Json.JsonConvert).FullName,Newtonsoft.Json.JsonConvert.SerializeObject,out string json_string));
            dates.Add(TestDeserialyze<F>(json_string,typeof(Newtonsoft.Json.JsonConvert).FullName, Newtonsoft.Json.JsonConvert.DeserializeObject<F>, out f2));

            // System.Text.Json 
            dates.Add(TestSerialyze<F>(f, typeof(System.Text.Json.JsonSerializer).FullName, System.Text.Json.JsonSerializer.Serialize<F>,out json_string));
            dates.Add(TestDeserialyze<F>(json_string, typeof(System.Text.Json.JsonSerializer).FullName, System.Text.Json.JsonSerializer.Deserialize<F>,out f2));

            /// Custom Class
            var custom1 = new CustomClass1()
                {
                    Prop1 = 1,
                    Prop2 = "Value of Prop2",
                    Prop3 = 3.5f,
                    Field1 = 2,
                    Field2 = "Value of Field2",
                    Field3 = 5.6f
                };
            dates.Add(TestSerialyze<CustomClass1>(custom1, typeof(CsvSerialyzer).FullName, serializer.Serialize, out csv_string));
            dates.Add(TestDeserialyze<CustomClass1>(csv_string, typeof(CsvSerialyzer).FullName, serializer.Deserialize<CustomClass1>, out CustomClass1 custom2));

            // Newtonsoft.Json;
            dates.Add(TestSerialyze<CustomClass1>(custom1, typeof(Newtonsoft.Json.JsonConvert).FullName, Newtonsoft.Json.JsonConvert.SerializeObject, out json_string));
            dates.Add(TestDeserialyze<CustomClass1>(json_string, typeof(Newtonsoft.Json.JsonConvert).FullName, Newtonsoft.Json.JsonConvert.DeserializeObject<CustomClass1>, out custom2));

            // System.Text.Json 
            dates.Add(TestSerialyze<CustomClass1>(custom1, typeof(System.Text.Json.JsonSerializer).FullName, System.Text.Json.JsonSerializer.Serialize<CustomClass1>, out json_string));
            dates.Add(TestDeserialyze<CustomClass1>(json_string, typeof(System.Text.Json.JsonSerializer).FullName, System.Text.Json.JsonSerializer.Deserialize<CustomClass1>, out custom2));

            PrintResults(dates);
            Console.ReadKey();
        }
        public static TestTime TestSerialyze<T>(T item, string SerialyzerName, Func<T,string> serialyze, out string output)
        {
            List<double> times = new List<double>();
            for (int i = 0; i < 1000; i++)
            {
                var date_start = DateTime.Now;
                serialyze.Invoke(item);
                var date_end = DateTime.Now;
                times.Add((date_end - date_start).TotalMilliseconds);
            }
            output = serialyze.Invoke(item);
            return new TestTime(typeof(T).Name,SerialyzerName,StatusOperation.Serialization,times.Average());
        }
        public static TestTime TestSerialyze<T>(T item, string SerialyzerName, Func<T, System.Text.Json.JsonSerializerOptions, string> serialyze, out string output)
        {
            List<double> times = new List<double>();
            for (int i = 0; i < 1000; i++)
            {
                var date_start = DateTime.Now;
                serialyze.Invoke(item, null);
                var date_end = DateTime.Now;
                times.Add((date_end - date_start).TotalMilliseconds);
            }
            output = serialyze.Invoke(item, null);
            return new TestTime(typeof(T).Name, SerialyzerName, StatusOperation.Serialization, times.Average());
        }
        public static TestTime TestDeserialyze<T>(string str, string SerialyzerName, Func<string, T> deserialyzer, out T item)
        {
            List<double> times = new List<double>();
            for (int i = 0; i < 1000; i++)
            {
                var date_start = DateTime.Now;
                deserialyzer.Invoke(str);
                var date_end = DateTime.Now;
                times.Add((date_end - date_start).TotalMilliseconds);
            }
            item = deserialyzer.Invoke(str);
            return new TestTime(typeof(T).Name, SerialyzerName,StatusOperation.Deserization,times.Average());
        }
        public static TestTime TestDeserialyze<T>(string str, string SerialyzerName, Func<string, System.Text.Json.JsonSerializerOptions, T> deserialyzer, out T item)
        {
            List<double> times = new List<double>();
            for (int i = 0; i < 1000; i++)
            {
                var date_start = DateTime.Now;
                deserialyzer.Invoke(str, null);
                var date_end = DateTime.Now;
                times.Add((date_end - date_start).TotalMilliseconds);
            }
            item = deserialyzer.Invoke(str, null);
            return new TestTime(typeof(T).Name, SerialyzerName, StatusOperation.Deserization, times.Average());
        }

        public static void PrintResults(List<TestTime> dates)
        {
            Console.WriteLine("+-------------------------------------------------------------------------------------+");
            Console.WriteLine("|                                       Output results                                |");
            Console.WriteLine("+--------------+------------------------------------------+---------------+-----------+");
            Console.WriteLine("|   Class name |                Serialyzer Name           |   Operation   |   Time(mc)|");
            Console.WriteLine("+--------------+------------------------------------------+---------------+-----------+");
            foreach (var date in dates)
            {
                Console.WriteLine($"| {date.ClassName,-12} | {date.SerialyzerName, -40} | {date.Status,13} |{String.Format("{0,10:N4}",date.Time), -10} |");    
                Console.WriteLine("+--------------+------------------------------------------+---------------+-----------+");
            }
        }
    }
}
