﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Databases.Data.Migrations
{
    public partial class Inital : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "People",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(type: "TEXT", nullable: true),
                    LastName = table.Column<string>(type: "TEXT", nullable: true),
                    Age = table.Column<int>(type: "INTEGER", nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: true),
                    Phone = table.Column<string>(type: "TEXT", nullable: true),
                    Address = table.Column<string>(type: "TEXT", nullable: true),
                    Discriminator = table.Column<string>(type: "TEXT", nullable: false),
                    StudentId = table.Column<int>(type: "INTEGER", nullable: true),
                    TeacherId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_People", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    DepartmentId = table.Column<int>(type: "INTEGER", nullable: false),
                    TeacherId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Courses_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Courses_People_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentTeacher",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(type: "INTEGER", nullable: false),
                    TeacherId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentTeacher", x => new { x.DepartmentId, x.TeacherId });
                    table.ForeignKey(
                        name: "FK_DepartmentTeacher_Departments_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Departments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepartmentTeacher_People_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourceStudent",
                columns: table => new
                {
                    CourseId = table.Column<int>(type: "INTEGER", nullable: false),
                    StudentId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourceStudent", x => new { x.CourseId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_CourceStudent_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourceStudent_People_StudentId",
                        column: x => x.StudentId,
                        principalTable: "People",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "C#" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "ASP.NET" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Java" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 4, "Android" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 5, "Microservices" });

            migrationBuilder.InsertData(
                table: "Departments",
                columns: new[] { "Id", "Name" },
                values: new object[] { 6, "C++" });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "StudentId" },
                values: new object[] { 2, "St.Peterburg", 22, "Student", "borsh22@gmail.com", "Dmitry", "Borchevskiy", "439-45-34", 1 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "StudentId" },
                values: new object[] { 5, "Varonehe", 18, "Student", "sser23dds@rambler.ru", "Sergey", "Mamontov", "232-45-83", 2 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "StudentId" },
                values: new object[] { 7, "Moscow", 25, "Student", "semen@otus.ru", "Semen", "Grishin", "123-737-34", 3 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "TeacherId" },
                values: new object[] { 1, "Moscow", 42, "Teacher", "ivanov@otus.ru", "Ivan", "Ivanov", "123-45-34", 1 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "TeacherId" },
                values: new object[] { 3, "Kiev", 32, "Teacher", "olga453@mail.ru", "Olga", "Semenova", "123-45-324", 2 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "TeacherId" },
                values: new object[] { 4, "Moscow", 54, "Teacher", "skher342@otus.ru", "Tatiana", "Dudikova", "438-83-34", 3 });

            migrationBuilder.InsertData(
                table: "People",
                columns: new[] { "Id", "Address", "Age", "Discriminator", "Email", "FirstName", "LastName", "Phone", "TeacherId" },
                values: new object[] { 6, "Dzankoy", 43, "Teacher", "nspr223@otus.ru", "Nikolay", "Spiridonov", "123-45-74", 4 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 2, 1, "C# advance", 2 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 4, 3, "Java basic", 4 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 6, 6, "C++ professional", 2 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 1, 1, "C# basic", 1 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 3, 1, "C# professional", 1 });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "DepartmentId", "Name", "TeacherId" },
                values: new object[] { 5, 4, "Android professional", 3 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 2, 4 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 5, 3 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 4, 3 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 6, 1 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 4, 1 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 3, 4 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 6, 2 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 3, 2 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 1, 2 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 5, 1 });

            migrationBuilder.InsertData(
                table: "DepartmentTeacher",
                columns: new[] { "DepartmentId", "TeacherId" },
                values: new object[] { 6, 4 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 2, 3 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 2, 2 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 6, 1 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 6, 2 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 3, 1 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 3, 2 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 5, 3 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 5, 1 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 4, 2 });

            migrationBuilder.InsertData(
                table: "CourceStudent",
                columns: new[] { "CourseId", "StudentId" },
                values: new object[] { 4, 3 });

            migrationBuilder.CreateIndex(
                name: "IX_CourceStudent_StudentId",
                table: "CourceStudent",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_DepartmentId",
                table: "Courses",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Courses_TeacherId",
                table: "Courses",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentTeacher_TeacherId",
                table: "DepartmentTeacher",
                column: "TeacherId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CourceStudent");

            migrationBuilder.DropTable(
                name: "DepartmentTeacher");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "People");
        }
    }
}
