﻿using Databases.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Databases.Data
{
    public class OtusDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Courses> Courses { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Person> People { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite(@"Data Source=.\otus.db;");
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            var students = new List<Student>
            {
                new Student { Id = 1, FirstName = "Dmitry",    LastName = "Borchevskiy", Age=22, Email = "borsh22@gmail.com",      Address = "St.Peterburg", Phone = "439-45-34" },
                new Student { Id = 2, FirstName = "Sergey",    LastName = "Mamontov",    Age=18, Email = "sser23dds@rambler.ru",   Address = "Varonehe",     Phone = "232-45-83" },
                new Student { Id = 3, FirstName = "Semen",     LastName = "Grishin",     Age=25, Email = "semen@otus.ru",          Address = "Moscow",       Phone = "123-737-34" }
            };

            var teachers = new List<Teacher>
            {
                new Teacher{Id = 4, FirstName = "Ivan",     LastName = "Ivanov",    Age=42,     Email = "ivanov@otus.ru",   Address = "Moscow", Phone = "123-45-34"},
                new Teacher{Id = 5, FirstName = "Olga",     LastName = "Semenova",  Age = 32,   Email = "olga453@mail.ru",  Address = "Kiev",   Phone = "123-45-324" },
                new Teacher{Id = 6, FirstName = "Tatiana",  LastName = "Dudikova",  Age = 54,   Email = "skher342@otus.ru", Address = "Moscow", Phone = "438-83-34" },
                new Teacher{Id = 7, FirstName = "Nikolay",  LastName = "Spiridonov",Age = 43,   Email = "nspr223@otus.ru",  Address = "Dzankoy",Phone = "123-45-74" }
            };
            var departments = new List<Department>
            {
                new Department{Id = 1, Name="C#"},
                new Department{Id = 2, Name="ASP.NET" },
                new Department{Id = 3, Name="Java" },
                new Department{Id = 4, Name="Android" },
                new Department{Id = 5, Name="Microservices" },
                new Department{Id = 6, Name="C++" }
            };

            var courses = new List<Courses>
            {
                new Courses{Id = 1, Name="C# basic",             DepartmentId = 1,   TeacherId = 1 },
                new Courses{Id = 2, Name="C# advance",           DepartmentId = 1,   TeacherId = 2 },
                new Courses{Id = 3, Name="C# professional",      DepartmentId = 1,   TeacherId = 1 },
                new Courses{Id = 4, Name="Java basic",           DepartmentId = 3,   TeacherId = 4 },
                new Courses{Id = 5, Name="Android professional", DepartmentId = 4,   TeacherId = 3 },
                new Courses{Id = 6, Name="C++ professional",     DepartmentId = 6,   TeacherId = 2 }
            };

            builder.Entity<Courses>().HasData(courses);
            builder.Entity<Student>()
                .HasMany(c => c.Courses)
                .WithMany(s => s.Students)
                .UsingEntity<Dictionary<string, object>>(
                "CourceStudent",
                course  =>  course.HasOne<Courses>().WithMany().HasForeignKey("CourseId"),
                student => student.HasOne<Student>().WithMany().HasForeignKey("StudentId"),
                cs =>
                {
                    cs.HasKey("CourseId", "StudentId");
                    cs.HasData(
                                new { StudentId = 1, CourseId = 1 },
                                new { StudentId = 1, CourseId = 2 },
                                new { StudentId = 3, CourseId = 2 },
                                new { StudentId = 2, CourseId = 2 },
                                new { StudentId = 1, CourseId = 3 },
                                new { StudentId = 2, CourseId = 3 },
                                new { StudentId = 2, CourseId = 4 },
                                new { StudentId = 3, CourseId = 4 },
                                new { StudentId = 3, CourseId = 5 },
                                new { StudentId = 1, CourseId = 5 },
                                new { StudentId = 1, CourseId = 6 },
                                new { StudentId = 2, CourseId = 6 }
                        );
                }).HasData(students);

            builder.Entity<Teacher>().HasData(teachers);

            builder.Entity<Department>()
                .HasMany(t => t.Teachers)
                .WithMany(d => d.Departments)
                .UsingEntity<Dictionary<string, object>>(
                    "DepartmentTeacher",
                    teacher => teacher.HasOne<Teacher>().WithMany().HasForeignKey("TeacherId"),
                    department => department.HasOne<Department>().WithMany().HasForeignKey("DepartmentId"),
                    td =>
                    {
                        td.HasKey("DepartmentId", "TeacherId");
                        td.HasData(
                                    new { DepartmentId = 1, TeacherId = 1 },
                                    new { DepartmentId = 1, TeacherId = 2 },
                                    new { DepartmentId = 2, TeacherId = 1 },
                                    new { DepartmentId = 2, TeacherId = 4 },
                                    new { DepartmentId = 3, TeacherId = 4 },
                                    new { DepartmentId = 3, TeacherId = 2 },
                                    new { DepartmentId = 4, TeacherId = 3 },
                                    new { DepartmentId = 4, TeacherId = 1 },
                                    new { DepartmentId = 5, TeacherId = 1 },
                                    new { DepartmentId = 5, TeacherId = 3 },
                                    new { DepartmentId = 6, TeacherId = 1 },
                                    new { DepartmentId = 6, TeacherId = 2 },
                                    new { DepartmentId = 6, TeacherId = 4 });
                    }).HasData(departments);
            base.OnModelCreating(builder);
        }
    }
}
