﻿using NUnit.Framework;
using System.Collections.Generic;
using Moq;

namespace Databases.Domain.Tests
{
    [TestFixture]
    public class CourseTests
    {
        private Courses Create
            (
                int Id = 1,
                string Name = "Cource 1",
                int DepartmentId = 1,
                Department Department = null,
                List<Student> Students = null,
                Teacher Teacher = null
            )
        {
            Department ??= new Mock<Department>().Object;
            Students ??= new List<Student>();
            Teacher ??= new Mock<Teacher>().Object;
            return new Courses
            {
                Id = Id,
                Name = Name,
                DepartmentId = DepartmentId,
                Department = Department,
                Students = Students,
                Teacher = Teacher
            };
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [Test] public void Name_IsNullByDefault_True() => Assert.That(new Courses().Name, Is.Null);
        [Test] public void Department_IsNullByDefault_True() => Assert.That(new Courses().Department, Is.Null);
        [Test] public void Students_IsNullByDefault_True() => Assert.That(new Courses().Students, Is.Null);
        [Test] public void Teacher_IsNullByDefault_True() => Assert.That(new Courses().Teacher, Is.Null);
        [Test]
        public void ToString_ReturnExpectedString_True()
        {

            string name = "Course 1";
            var department = new Department
            {
                Name = "Department 1"
            };
            var teacher = new Teacher 
            {
                LastName = "Ivanov"
            }; 

            var course = Create(Name:name, Department:department, Teacher: teacher);
            var expected = $"Cource Name: {name}; Department : Department 1; Teacher: Ivanov";
            Assert.That(course.ToString(), Is.EqualTo(expected));
        }
    }
}
