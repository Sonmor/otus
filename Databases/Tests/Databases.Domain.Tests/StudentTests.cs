﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;

namespace Databases.Domain.Tests
{
    public class StudentTests
    {
        private Student Create(
                              string FirstName = null,
                              string LastName = null,
                              string Phone = null,
                              string Address = null,
                              string Email = null,
                              int Age = 0,
                              List<Courses> Cources = null)
        {
            FirstName ??= "Homer";
            LastName ??= "Simpson";
            Phone ??= "123-43-4566";
            Email ??= "homer.simpson@mail.com";
            Address ??= "Stivenson drive";
            Age = Age > 0 ? Age : 0;
            Cources ??= new List<Courses>();

           return new Student()
            {
                FirstName = FirstName,
                LastName = LastName,
                Age = Age,
                Phone = Phone,
                Address = Address,
                Email = Email,
                Courses = Cources,
                Id = 1,
                StudentId = 1
            };
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test] public void FirstName_IsNullByDefault_True() => Assert.That(new Student().FirstName, Is.Null);
        [Test] public void LastName_IsNullByDefault_True() => Assert.That(new Student().LastName, Is.Null);
        [Test] public void Email_IsNullByDefault_True() => Assert.That(new Student().Email, Is.Null);

        [Test]
        public void ToString_ReturnExpectedString_True()
        {
            var FirstName = "Homer";
            var LastName = "Simpson";
            var Phone = "123-43-4566";
            var Email = "homer.simpson@mail.com";
            var Address = "Stivenson drive";
            var Age = 42;
            var Cources = new List<Courses>
            {
                new Courses {Id = 1, Name = "Cource 1", Department = new Department{Id=1,Name = "Department 1", Teachers = new List<Teacher>() }},
                new Courses {Id = 2, Name = "Cource 2", Department = new Department{Id=1,Name = "Department 1", Teachers = new List<Teacher>() }},
                new Courses {Id = 3, Name = "Cource 3", Department = new Department{Id=1,Name = "Department 1", Teachers = new List<Teacher>() }},
                new Courses {Id = 4, Name = "Cource 4", Department = new Department{Id=2,Name = "Department 2", Teachers = new List<Teacher>() }},
                new Courses {Id = 5, Name = "Cource 5", Department = new Department{Id=2,Name = "Department 2", Teachers = new List<Teacher>() }}
            };

            var student = Create
                (
                    FirstName: FirstName,
                    LastName: LastName,
                    Phone: Phone,
                    Email: Email,
                    Address: Address,
                    Age: Age,
                    Cources : Cources
                );
            var expected = $"{FirstName}\t{LastName}\t{Age}\t{Address}\t{Phone}\tStudent cources:\r\n\t - Cource 1 - Department 1\r\n\t - Cource 2 - Department 1\r\n\t - Cource 3 - Department 1\r\n\t - Cource 4 - Department 2\r\n\t - Cource 5 - Department 2\r\n";
            Assert.That(student.ToString(), Is.EqualTo(expected));
        }
    }
}