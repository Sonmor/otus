﻿using System;
using System.Linq;
using Databases.Data;
using Databases.Domain;

namespace Databases.Inf.Managers
{
    public abstract class ManagerBase<T>
    {
        protected readonly OtusDbContext context;
        protected ManagerBase(OtusDbContext context)
            => this.context = context;

        public void AddItem(T item, IDatabaseApp app)
        { 
            try
            {
                context.Add(item);
                context.SaveChanges();
                app.OnWriteMessage($"{typeof(T).Name} {item} added to DB");
            }
            catch(Exception ex)
            {
                app.OnWriteError(ex.Message);
            }
        }
        public void RemoveItem(int id, IDatabaseApp app)
        {
            try
            {
                T item = GetItem(id);
                
                context.Remove(item);
                context.SaveChanges();
                app.OnWriteMessage($"{typeof(T).Name} {item} removed from DB ");
            }
            catch
            {
                app.OnWriteError($"Cannot find and remove items with Id = {id}");
            }
        }
        protected abstract IQueryable<T> GetItems();
        protected abstract T GetItem(int id);
        public void ShowItems(IDatabaseApp app)
        {
            try
            {
                foreach (var item in GetItems())
                    app.OnWriteMessage($"{item}\n");
            }
            catch(Exception ex)
            {
                app.OnWriteError(ex.Message);
            }
        }
    }
}
