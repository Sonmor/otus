﻿using System.Linq;
using Databases.Data;
using Databases.Domain;
using Microsoft.EntityFrameworkCore;

namespace Databases.Inf.Managers
{
    public class DepartmentsManager:ManagerBase<Department>
    {
        public DepartmentsManager(OtusDbContext context) : base(context) { }
        protected override Department GetItem(int id) => context.Departments.First(i => i.Id == id);

        protected override IQueryable<Department> GetItems() 
            => context.Departments
            .AsNoTracking()
            .Include(t => t.Teachers);
    }
}
