﻿using Databases.Data;
using Databases.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Databases.Inf.Managers
{
    public class StudentsManager : ManagerBase<Student>
    {
        public StudentsManager(OtusDbContext context) : base(context) { }
        protected override Student GetItem(int id) => context.Students.First(i => i.Id == id);
        protected override IQueryable<Student> GetItems() 
            => context.Students
            .AsNoTracking()
            .Include(c => c.Courses)
            .ThenInclude(d => d.Department);
    }
}