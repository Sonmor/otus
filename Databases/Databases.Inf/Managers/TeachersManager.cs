﻿using System.Linq;
using Databases.Data;
using Databases.Domain;
using Microsoft.EntityFrameworkCore;

namespace Databases.Inf.Managers
{
    public class TeachersManager:ManagerBase<Teacher>
    {
        public TeachersManager(OtusDbContext context) : base(context) { }
        protected override Teacher GetItem(int id) => context.Teachers.First(i => i.Id == id);

        protected override IQueryable<Teacher> GetItems() 
            => context.Teachers
            .AsNoTracking()
            .Include(c => c.Courses)
            .ThenInclude(d => d.Department)
            .Include(d => d.Departments);
    }
}
