﻿using Databases.Data;
using Databases.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Databases.Inf.Managers
{
    public class CoursesManager : ManagerBase<Courses>
    {
        public CoursesManager(OtusDbContext context) : base(context) { }
        protected override Courses GetItem(int id) => context.Courses.First( i => i.Id == id);
        protected override System.Linq.IQueryable<Courses> GetItems() 
            => context.Courses
            .AsNoTracking()
            .Include(d => d.Department)
            .Include(s => s.Students)
            .Include(t => t.Teacher);
    }
}
