﻿using System;
using Databases.Data;
using Databases.Domain;
using Databases.Inf.Managers;

namespace Databases.Inf
{
    public class DatabaseApp : IDatabaseApp
    {
        private StudentsManager students;
        private TeachersManager teachers;
        private CoursesManager courses;
        private DepartmentsManager departments;

        public event Func<string> ReadData;
        public event Action<string> WriteMessage;
        public event Action<string> WriteError;
        public event Action StartApp;
        public event Action FinishApp;

        internal event Action<Student,IDatabaseApp> AddStudentEvent;
        internal event Action<int,IDatabaseApp> RemoveStudentEvent;

        public event Action<Teacher, IDatabaseApp> AddTeacherEvent;
        public event Action<int, IDatabaseApp> RemoveTeacherEvent;

        internal event Action<IDatabaseApp> ShowStudentsListEvent;
        internal event Action<IDatabaseApp> ShowTeachersListEvent;
        internal event Action<IDatabaseApp> ShowCoursesListEvent;
        internal event Action<IDatabaseApp> ShowDepartmentsListEvent;

        public string OnReadData() => ReadData.Invoke();
        public void OnWriteMessage(string message) => WriteMessage.Invoke(message);
        public void OnWriteError(string error) => WriteError.Invoke(error);
        public void OnInit()
        {
            OtusDbContext context = new ();
            context.Database.EnsureCreated();

            students = new(context);
            teachers = new(context);
            courses = new(context);
            departments = new(context);

            AddStudentEvent += students.AddItem;
            RemoveStudentEvent += students.RemoveItem;
            ShowStudentsListEvent += students.ShowItems;

            AddTeacherEvent += teachers.AddItem;
            RemoveTeacherEvent += teachers.RemoveItem;
            ShowTeachersListEvent += teachers.ShowItems;

            ShowCoursesListEvent += courses.ShowItems;
            ShowDepartmentsListEvent += departments.ShowItems;
        }

        public void OnStart() => StartApp();
        public void OnAddStudent(Student student) => AddStudentEvent(student ,this);
        public void OnRemoveStudent(int id) => RemoveStudentEvent(id, this);
        public void OnAddTeacher(Teacher teacher) => AddTeacherEvent(teacher, this);
        public void OnRemoveTeacher(int id) => RemoveTeacherEvent(id, this);
        public void OnShowStudentsList() => ShowStudentsListEvent(this);
        public void OnShowTeachersList() => ShowTeachersListEvent(this);
        public void OnShowDepartments() => ShowDepartmentsListEvent(this);
        public void OnShowCources() => ShowCoursesListEvent(this);
        public void OnFinish()
        {
            AddStudentEvent -= students.AddItem;
            RemoveStudentEvent -= students.RemoveItem;
            ShowStudentsListEvent -= students.ShowItems;

            AddTeacherEvent -= teachers.AddItem;
            RemoveTeacherEvent -= teachers.RemoveItem;
            ShowTeachersListEvent -= teachers.ShowItems;

            ShowCoursesListEvent -= courses.ShowItems;
            ShowDepartmentsListEvent -= departments.ShowItems;
            FinishApp();
        }
    }
}
