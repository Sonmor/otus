using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Databases.Domain
{
    public class Courses
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        // On to many 
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        /// <summary>
        /// Many to many relationship
        /// </summary>
        public List<Student> Students { get; set; }

        internal static bool Any()
        {
            throw new NotImplementedException();
        }

        // One to many
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public override string ToString() => $"Cource Name: {Name}; Department : {Department.Name}; Teacher: {Teacher.LastName}";
    }
}