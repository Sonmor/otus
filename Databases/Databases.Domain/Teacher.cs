using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Databases.Domain
{
    public class Teacher : Person
    {
        public List<Courses> Courses { get; set; }
        public List<Department> Departments { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new($"Teacher:\n ID: {Id}\n{ base.ToString() }");
            if (Courses != null && Courses.Any())
            {
                builder.AppendLine("\r\nCources:");
                foreach (var cource in Courses)
                    builder.AppendLine($"\t - {cource.Name} - {cource.Department.Name}");
            }
            return builder.ToString();
        }
    }
}