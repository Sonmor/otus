using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Databases.Domain
{
  public class Department
  {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id{get;set;}
        public string Name{get;set;}
        public List<Teacher> Teachers{get;set;}
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder($"Department: {Name};\r\nTeachers:\n");
            foreach (var teacher in Teachers)
                builder.AppendLine($"  - {teacher.LastName} {teacher.FirstName}");

            return builder.ToString();
        }

    }
}