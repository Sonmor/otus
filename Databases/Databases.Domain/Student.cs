﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Databases.Domain
{
    public class Student : Person
    {
        public List<Courses> Courses { get; set; } = new List<Courses>();
        public override string ToString()
        {
            StringBuilder builder = new($"Student:\n ID: {Id}\n{ base.ToString() }");
            if (Courses != null && Courses.Any())
            {
                builder.AppendLine("\r\nCources:");
                foreach (var cource in Courses)
                    builder.AppendLine($"\t - {cource.Name} - {cource.Department.Name}");
            }
            return builder.ToString();
        }
    }
}
