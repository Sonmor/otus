﻿using System;

namespace Databases.Domain
{
    public interface IDatabaseApp
    {
        event Action FinishApp;
        event Func<string> ReadData;
        event Action StartApp;
        event Action<string> WriteError;
        event Action<string> WriteMessage;

        void OnFinish();
        string OnReadData();
        void OnStart();
        void OnWriteError(string error);
        void OnWriteMessage(string message);
    }
}