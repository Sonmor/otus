﻿using System;
using System.Linq;
using Databases.Inf;
using Databases.Domain;

namespace Databases.CUI
{
    internal class Program
    {

        static void Main(string[] args)
        {
            Program prog = new(new DatabaseApp());
            prog.OnStart();
        }

        private readonly DatabaseApp app;
        private readonly ConsoleReader reader = new ConsoleReader();
        private readonly ConsoleWriter writer = new ConsoleWriter();
        private readonly ConsoleError error = new ConsoleError();
        private readonly ConsoleMenu menu = new ConsoleMenu();
        internal Program(DatabaseApp app)
        {
            this.app = app;
            menu.MenuItems.Add(new Tuple<int, string, Action>(0, "Exit", Finish));
            menu.MenuItems.Add(new Tuple<int, string, Action>(1, "Add a Student",OnAddStudent));
            menu.MenuItems.Add(new Tuple<int, string, Action>(2, "Remove Student", OnRemoveStudent));
            menu.MenuItems.Add(new Tuple<int, string, Action>(3, "Add a Teacher", OnAddTeacher));
            menu.MenuItems.Add(new Tuple<int, string, Action>(4, "Remove Teacher", OnRemoveTeacher));
            menu.MenuItems.Add(new Tuple<int, string, Action>(5, "Show list of Students", OnShowStudents));
            menu.MenuItems.Add(new Tuple<int, string, Action>(6, "Show list of Teachers", OnShowTeachers));
            menu.MenuItems.Add(new Tuple<int, string, Action>(7, "Show list of Otus department", OnShowDepartments));
            menu.MenuItems.Add(new Tuple<int, string, Action>(8, "Show list of Cources", OnShowCourses)); 

            app.ReadData += reader.Read;
            app.WriteMessage += writer.Write;
            app.WriteError += error.ThrowError;
            app.StartApp += OnStart;

            app.OnInit();
            app.OnStart();
        }

        internal void OnStart()
        {

            writer.Write("+-------------------------+\n");
            writer.Write("|    Welcome to Database  |\n");
            writer.Write("+-------------------------+\n");
            writer.Write("Please press any key ...");
            _ = reader.Read();
            do
            {
                reader.Clear();
                _= menu.DisplayMenu();
                if (!int.TryParse(reader.Read(), out int num))
                {
                    app.OnWriteError("Incorrect value, please sende again ... \n");
                    continue;
                }
                try
                {
                    menu.MenuItems.First(x => x.Item1 == num).Item3();
                }
                catch
                {
                    app.OnWriteError($"Our menu havn't number {num}. Please send another number..\n");
                }
                writer.Write("Press any key to continue ...");
                _ = reader.Read();
            } while (true);
        }
        internal void Finish()
        {
            app.OnWriteMessage("+-------------------------+\n");
            app.OnWriteMessage("| Database app is closed  |\n");
            app.OnWriteMessage("+-------------------------+\n");
            app.OnWriteMessage("Please press any key to exit ...");
            _ = app.OnReadData();

            app.ReadData -= reader.Read;
            app.WriteMessage -= writer.Write;
            app.WriteError -= error.ThrowError;

            Environment.Exit(0);
        }
        internal void OnShowStudents() => app.OnShowStudentsList();
        internal void OnShowTeachers() => app.OnShowTeachersList();
        internal void OnShowCourses() => app.OnShowCources();
        internal void OnShowDepartments() => app.OnShowDepartments();
        internal T CreatePerson<T>() where T :Person, new()
        {
            writer.Write("Please insert person First Name: ");
            string first = reader.Read();
            writer.Write("Please insert person Last Name: ");
            string last = reader.Read();
            writer.Write("Please insert person Age: ");
            int.TryParse(reader.Read(), out int age);

            writer.Write("Please insert person email: ");
            string email = reader.Read();

            writer.Write("Please insert person Phone number: ");
            string phone = reader.Read();

            writer.Write("Please insert person Adderss: ");
            string address = reader.Read();
            return new()
            {
                FirstName = first,
                LastName = last,
                Age = age,
                Email = email,
                Address = address,
                Phone = phone
            };
        }
        internal void OnAddStudent()
        {
            writer.Write("+-------------------------+\n");
            writer.Write("|    Add new Student      |\n");
            writer.Write("+-------------------------+\n");
            writer.Write("Please provide some information about new student:\n");

            try
            {
                Student student = CreatePerson<Student>();
                app.OnAddStudent(student);
            }
            catch (Exception ex)
            {
                error.ThrowError(ex.Message);
            }
        }


        internal void OnRemoveStudent()
        {
            writer.Write("+-------------------------+\n");
            writer.Write("|    Remove  Student      |\n");
            writer.Write("+-------------------------+\n");
            writer.Write("Please insert Student Id: ");
            try
            {
                int.TryParse(reader.Read(), out int id);
                app.OnRemoveStudent(id);
            }
            catch(Exception ex)
            {
                error.ThrowError(ex.Message);
            }
        }
        internal void OnAddTeacher()
        {
            writer.Write("+-------------------------+\n");
            writer.Write("|    Add new Teacher      |\n");
            writer.Write("+-------------------------+\n");
            writer.Write("Please provide some information about new teacher:\n");

            try
            {
                Teacher teacher = CreatePerson<Teacher>();
                app.OnAddTeacher(teacher);
            }
            catch(Exception ex)
            {
                error.ThrowError(ex.Message);
            }
        }
        internal void OnRemoveTeacher()
        {
            writer.Write("+-------------------------+\n");
            writer.Write("|    Remove  Teacher      |\n");
            writer.Write("+-------------------------+\n");
            writer.Write("Please insert Teacher Id: ");
            try
            {
                int.TryParse(reader.Read(), out int id);
                app.OnRemoveTeacher(id);
            }
            catch(Exception ex)
            {
                error.ThrowError(ex.Message);
            }
        }
    }
}
