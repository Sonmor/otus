﻿using System;

namespace Databases.CUI
{
    public class ConsoleWriter
    {
        public void Write(string message) => Console.Write(message);
    }
}
