﻿using System;

namespace Databases.CUI
{
    public class ConsoleError
    {
        public void ThrowError(string error) => Console.Write($"Error: {error}");
    }
}
