﻿using System;

namespace Databases.CUI
{
    public class ConsoleReader
    {
        public string Read() =>Console.ReadLine();
        public void Clear() => Console.Clear();
    }
}
