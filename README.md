# Домашние задания по курсу OTUS "C# Developer. Professional"

В рамках данного курса были выполнены следующие практические занятия:
 - [Создаем наборы классов и интерфейсов](./Interfaces/)
 - [Обрабатываем данные цепью методов](https://gitlab.com/Sonmor/otus.teaching.linq.atm)
 - [Список URL адрессов](./Regex/)
 - [Рефлексия и ее применение](./Reflection/)
 - [Подключаем базу данных к проекту](./Databases/)
 - [Делегаты и события](./Events/)
 - [Параллельная загрузка данных из файла](https://gitlab.com/Sonmor/concurrency-import)
 - [Делаем проект многопоточным](./Parallel.Import/)
 - [Реализуем паттерн Prototype](./Cloneable/)
 - [Взаимодействие между клиентом и сервером](./WebAPI/)
 - [SOLID](./SOLID/)
