﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace SOLID.Domain.Tests
{
    [TestFixture]
    public class SettingsTests
    {
        private Settings Create(IGameApp app = null) 
        {
            app ??= new Mock<IGameApp>().Object;

            return new Settings(app);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test] public void StepCount_AreEqualSendedValue_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            Settings settings = Create(app:app.Object);
            
            settings.SetStepCount();
            Assert.That(settings.Steps,Is.EqualTo(10));
        }

        [Test] 
        public void StepCount_AreCallAppReadData_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            var settings = Create(app: app.Object);
            settings.SetStepCount();
            app.Verify(x => x.ReadData(), Times.Once);
        }
        [Test]
        public void StepCount_SendIncorrectData_CallAppWriteError()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("q");
            var settings = Create(app: app.Object);
            settings.SetStepCount();
            app.Verify(x => x.WriteError(It.IsAny<string>()), Times.Once);
        }
        [Test]
        public void StepMinRange_AreEqualSendedValue_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("5");
            Settings settings = Create(app: app.Object);

            settings.SetRangeMininum();
            Assert.That(settings.Range.Min, Is.EqualTo(5));
        }
        [Test]
        public void StepMinRange_AreCallAppReadDate_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            var settings = Create(app: app.Object);
            settings.SetRangeMininum();
            app.Verify(x => x.ReadData(), Times.Once);
        }
        [Test]
        public void SetMinRange_SendIncorrectValue_CallAppWriteError()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("q");
            var settings = Create(app: app.Object);
            settings.SetRangeMininum();
            app.Verify(x => x.WriteError(It.IsAny<string>()), Times.Once);
        }
        [Test]
        public void StepMaxRange_AreEqualSendedValue_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            Settings settings = Create(app: app.Object);

            settings.SetRangeMaximum();
            Assert.That(settings.Range.Max, Is.EqualTo(10));
        }
        [Test]
        public void StepMaxRange_AreCallAppReadDate_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            var settings = Create(app: app.Object);
            settings.SetRangeMaximum();
            app.Verify(x => x.ReadData(), Times.Once);
        }
        [Test]
        public void SetMaxRange_SendIncorrectValue_CallAppWriteError()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("q");
            var settings = Create(app: app.Object);
            settings.SetRangeMaximum();
            app.Verify(x => x.WriteError(It.IsAny<string>()), Times.Once);
        }
    }
}
