﻿using NUnit.Framework;
using System;

namespace SOLID.Domain.Tests
{
    [TestFixture]
    public class StepRangeTests
    {
        private StepRange Create(int min = 0, int max = 10) => new StepRange() { Min=min, Max = max};

        [Test] public void Min_AreEqualSendedValue_True() => Assert.That(Create(1).Min, Is.EqualTo(1));

        [Test] public void Max_AreEqualSendedValue_True() => Assert.That(Create(max: 10).Max, Is.EqualTo(10));
    }
}
