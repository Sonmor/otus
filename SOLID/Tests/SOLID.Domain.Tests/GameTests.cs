﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace SOLID.Domain.Tests
{
    [TestFixture]
    public class GameTests
    {
        private Game Create(
            IGameApp app = null,
            IStepAnalyzer analyzer = null,
            int secret = 5,
            int steps = 10)
        {
            app ??= new Mock<IGameApp>().Object;
            analyzer ??= new Mock<IStepAnalyzer>().Object;
            return new Game(app: app,analyzer:analyzer, secret: secret, steps: steps);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [TestCase(4, 5, -1)]
        [TestCase(5, 4, 1)]
        [TestCase(4, 4, 0)]
        public void CompareNumber_ReturnCorrectValue_True(int number, int secret, int result)
        {
            var game = Create(secret: secret);
            Assert.That(game.CompareNumber(number), Is.EqualTo(result));
        }

        [Test]
        public void InputNumber_SendCorectValue_CallApplicationReadData()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");

            var game = Create(app: app.Object);
            _ = game.InputNumber(1);
            app.Verify(x => x.ReadData(), Times.Once);
        }

        [Test]
        public void InputNumber_ReturnExpectedValue_True()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("10");
            var game = Create(app: app.Object);
            Assert.That(game.InputNumber(1), Is.EqualTo(10));
        }

        [Test]
        public void InputNumber_SendX_CallApplicationFinishEvent()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("x");

            var game = Create(app: app.Object);
            _ = game.InputNumber(1);
            app.Verify(x => x.OnFinish(It.IsAny<string>()), Times.Once);
        }
        [Test]
        public void Play_SendSecretNumber_CallFinishEvent()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("5");
            var game = Create(app: app.Object, secret: 5, steps: 1);

            game.Play();
            app.Verify(x => x.OnFinish(It.IsAny<string>()), Times.Once);
        }
        [Test]
        public void Play_SpentAllSteps_CallFinishEvent()
        {
            var app = new Mock<IGameApp>();
            app.Setup(x => x.ReadData()).Returns("5");
            var game = Create(app: app.Object, secret: 15, steps: 1);

            game.Play();
            app.Verify(x => x.OnFinish(It.IsAny<string>()), Times.Once);
        }

    }
}
