using Moq;
using NUnit.Framework;

namespace SOLID.Domain.Tests
{
    [TestFixture]
    public class StepAnalyzerTest
    {
        private StepAnalyzer Create(INumberAnalyzerStrategy strategy = null)
        {
            if (strategy == null) strategy = new Mock<INumberAnalyzerStrategy>().Object;
            return new StepAnalyzer(strategy);
        }

        [Test] public void CTOR_CreatedObjectIsNotNull_True() => Assert.That(Create(), Is.Not.Null);
        [TestCase(true)]
        [TestCase(false)]
        public void Analize_RetrieveValueAreEqualStrategyValue_True(bool expected)
        {
            var strategy = new Mock<INumberAnalyzerStrategy>();
            strategy
                .Setup(x => x.Analyze(It.IsAny<int>()))
                .Returns(expected);

            var analyzer = Create(strategy.Object);
            Assert.That(analyzer.Analyze(-10), Is.EqualTo(expected));

        }
    }
}