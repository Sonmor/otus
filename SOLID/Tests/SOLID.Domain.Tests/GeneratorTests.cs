﻿using NUnit.Framework;


namespace SOLID.Domain.Tests
{
    [TestFixture]
    public class GeneratorTests
    {
        private Generator Create() => Create(new StepRange() { Min=0, Max=10});
        private Generator Create(StepRange range) => new Generator(range);

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);

        [Test]
        public void Generate_RetrieveValuesAreNotEquals_True()
        {
            var generator = Create();
            var num1 = generator.Generate();
            var num2 = generator.Generate();
            Assert.That(num1, Is.Not.EqualTo(num2));
        }
        [Test]
        public void Generate_IncludeLowerAndUpperBoundies_True()
        {
            var generator = Create(new StepRange() { Min = 0, Max = 0 });
            var num1 = generator.Generate();
            var num2 = generator.Generate();
            Assert.That(num1, Is.EqualTo(num2));
        }
        [TestCase(0,1)]
        [TestCase(5, 10)]
        [TestCase(9,10)]
        [TestCase(-5, -4)]
        public void Generate_ValuesAccordingRange_True(int min, int max)
        {
            var generator = Create(new StepRange { Min = min, Max = max});
            var num1 = generator.Generate();
            Assert.That(num1, Is.InRange(min, max));
        }
    }
}
