﻿using NUnit.Framework;

namespace SOLID.Domain.NumberCheckStategies.Tests
{
    [TestFixture]
    public class NumberBetweenIncludeTests
    {
        private NumberBetweenInclude Create(StepRange range) => new(range);
        private NumberBetweenInclude Create() => Create(new StepRange{Min= 0, Max= 10});

        [Test] public void CTOR_CreatedObjectIsNotNull_True() => Assert.That(Create(), Is.Not.Null);
        [TestCase(0, 10, 4, true)]
        [TestCase(0, 10, 0, true)]
        [TestCase(0, 10, 10, true)]
        [TestCase(0, 10, -1, false)]
        [TestCase(0, 10, 11, false)]
        public void Analyzer_RetrievResultAreEqualExpected_True(int start, int end, int number, bool expect)
            => Assert.That(Create(new StepRange{Min= start, Max= end}).Analyze(number), Is.EqualTo(expect));
    }
}
