﻿namespace SOLID.Domain
{
    public interface IStepAnalyzer
    {
        bool Analyze(int step);
    }
}