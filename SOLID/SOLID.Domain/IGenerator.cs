﻿namespace SOLID.Domain
{
    public interface IGenerator
    {
        int Generate();
    }
}