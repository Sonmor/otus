﻿using System;

namespace SOLID.Domain
{
    public class Generator : IGenerator
    {
        private StepRange range;
        public Generator(StepRange range)
        {
            this.range = range;
        }
        public int Generate() => new Random().Next(range.Min, range.Max + 1);
    }
}