﻿using System;

namespace SOLID.Domain
{
    public class Game
    {
       

        private IStepAnalyzer analyzer { get; set; }
        private IGameApp app { get; set; }
        private readonly int secret;
        private readonly int steps;
        public Game(IGameApp app, IStepAnalyzer analyzer, int secret, int steps)
        {
            this.app = app;
            this.analyzer = analyzer;
            this.secret = secret; 
            this.steps = steps;
        }

        public void Play()
        {
            int count = 1;
            while (count <= steps)
            {
                int num = InputNumber(count);
                switch (CompareNumber(num))
                {
                    case -1:
                        app.WriteMessage($"Secret number more than {num} \n");
                        break;
                    case 1:
                        app.WriteMessage($"Secret number less than {num} \n");
                        break;
                    case 0:
                        app.OnFinish(" Congratulations!!!\n     You Win!!!\n");
                        return;
                }
                count++;
            }
            app.OnFinish("I'm sorry. You loose.\n");
        }

        internal int CompareNumber(int num)
        {
            if (num < secret) return -1;
            if (num > secret) return 1;
            return 0;
        }

        internal int InputNumber(int count)
        {
            int num = 0;
            string str;
            bool IsCorrectNumber = false;
            bool InRange = false;
            do
            {
                app.WriteMessage($"Step {count}/{steps}: Try guess number: (or x for exit): ");
                str = app.ReadData();
                if (str.ToLower().Equals("x"))
                {
                    app.OnFinish("Bye, bye ...\n");
                    break;
                }
                IsCorrectNumber = int.TryParse(str, out num);
                if (!IsCorrectNumber) app.WriteError("Please insert correct number\n");

                InRange = analyzer.Analyze(num);
                if (!InRange) app.WriteError("Please insert number according range\n");

            } while (!IsCorrectNumber || !InRange);
            return num;
        }
    }
}
