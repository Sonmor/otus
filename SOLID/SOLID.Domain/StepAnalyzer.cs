﻿namespace SOLID.Domain
{
    public class StepAnalyzer : IStepAnalyzer
    {
        protected INumberAnalyzerStrategy strategy;
        public StepAnalyzer(INumberAnalyzerStrategy strategy)
        {
            this.strategy = strategy;
        }
        public bool Analyze(int step) => strategy.Analyze(step);
    }
}
