﻿namespace SOLID.Domain.NumberCheckStategies
{
    public class NumberBetweenInclude : INumberAnalyzerStrategy
    {
        private readonly StepRange range;
        public NumberBetweenInclude(StepRange range)
        {
            this.range = range;
        }
        public bool Analyze(int step) => step >= range.Min && step <= range.Max;

    }
}
