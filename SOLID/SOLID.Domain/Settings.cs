﻿using System;

namespace SOLID.Domain
{
    public class Settings
    {
        private int _count = 0;
        public int Steps { get => _count; }

        public StepRange Range { get; set; }
        public IStepAnalyzer Analyzer { get; }

        private readonly IGameApp app;
        public Settings(IGameApp app)
        {
            this.app = app;
            this.Range = new StepRange();
        }

        public bool SetStepCount()
        {
            app.WriteMessage("Please insert maximum number of steps: ");
            try
            {
                _count = int.Parse(app.ReadData());
            }
            catch (Exception ex)
            {
                app.WriteError(ex.Message);
                return false;
            }
            return true;
        }
        public bool SetRange()
        {
            if(!SetRangeMininum() || !SetRangeMaximum()) return false;
            return true;
        }

        public bool SetRangeMininum()
        {
            int min = 0;
            app.WriteMessage("Please insert minimum number of range: ");
            
            if(!int.TryParse(app.ReadData(), out min))
            {
                app.WriteError("Incorrect value for mininum step range\n");
                return false;
            }

            /// For fix Compiler Error CS1612
            var range = Range;
            range.Min = min;
            Range = range;
            
            return true;
        }

        public bool SetRangeMaximum()
        {
            app.WriteMessage("Please insert maximum number of range: ");
            int max = 0;
            if (!int.TryParse(app.ReadData(), out max))
            {
                app.WriteError("Incorrect value for maximum step range\n");
                return false;
            }
            /// For fix Compiler Error CS1612
            var range = Range;
            range.Max = max;
            Range = range;
            return true;
        }
    }
}
