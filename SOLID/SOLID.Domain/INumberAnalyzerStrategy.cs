﻿namespace SOLID.Domain
{
    public interface INumberAnalyzerStrategy
    {
        public bool Analyze(int step);
    }
}
