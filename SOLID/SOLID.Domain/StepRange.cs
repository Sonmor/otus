﻿namespace SOLID.Domain
{
    public struct StepRange
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
