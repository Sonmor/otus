﻿namespace SOLID.Domain
{
    public interface IGameApp
    {
        void OnFinish(string message);
        void OnInit();
        void OnStart();
        string ReadData();
        void WriteError(string error);
        void WriteMessage(string message);
    }
}