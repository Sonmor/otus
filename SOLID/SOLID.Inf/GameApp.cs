﻿using System;
using SOLID.Domain;
using SOLID.Domain.NumberCheckStategies;

namespace SOLID.Inf
{
    public class GameApp : IGameApp
    {
        public event Action Start;
        public event Action<string> Finish;
        public event Func<string> InputData;
        public event Action<string> OutputData;
        public event Action<string> OutputError;

        private Settings settings;
        private StepAnalyzer analyzer;

        public void OnInit()
        {
            settings = new Settings(this);
            settings.SetRange();
            settings.SetStepCount();
            analyzer = new StepAnalyzer(new NumberBetweenInclude(settings.Range));
        }
        public void OnStart()
        {
            Start.Invoke();
            var game = new Game(app:this, analyzer: analyzer, secret: new Generator(settings.Range).Generate(), settings.Steps);
            game.Play();
        }
        public void OnFinish(string message) => Finish.Invoke(message);
        public void WriteMessage(string message) => OutputData.Invoke(message);
        public void WriteError(string error) => OutputError.Invoke(error);
        public string ReadData() => InputData.Invoke();
    }
}
