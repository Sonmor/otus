﻿namespace SOLID.Inf
{
    public interface IReader
    {
        string Read();
    }
}
