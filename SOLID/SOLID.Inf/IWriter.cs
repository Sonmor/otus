﻿namespace SOLID.Inf
{
    public interface IWriter
    {
        void Write(string message);
    }
}
