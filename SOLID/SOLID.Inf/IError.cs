﻿namespace SOLID.Inf
{
    public interface IError
    {
        void ThrowError(string error);
    }
}
