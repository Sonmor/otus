﻿using SOLID.Inf;
using System;

namespace SOLID.CUI
{
    internal class DataReader : IReader
    {
        public string Read() => Console.ReadLine();
    }
}