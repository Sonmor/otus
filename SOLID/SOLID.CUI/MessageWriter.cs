﻿using SOLID.Inf;
using System;

namespace SOLID.CUI
{
    public class MessageWriter : IWriter
    {
        public void Write(string message) => Console.Write(message);
    }
}
