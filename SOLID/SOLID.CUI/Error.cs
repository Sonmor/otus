﻿using SOLID.Inf;
using System;

namespace SOLID.CUI
{
    public class Error : IError
    {
        public void ThrowError(string error) => Console.WriteLine($"Error: {error}");
    }
}
