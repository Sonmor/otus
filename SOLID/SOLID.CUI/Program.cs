﻿using SOLID.Inf;
using System;

namespace SOLID.CUI
{
    class Program
    {
        static void Main()
        {
            _ = new Program();
        }


        private readonly GameApp app;
        private readonly Error error;
        private readonly MessageWriter writer;
        private readonly DataReader reader;

        public Program()
        {
            app = new();
            error = new();
            writer = new();
            reader = new();

            app.Start += OnStart;
            app.Finish += OnFinish;
            app.InputData += reader.Read;
            app.OutputData += writer.Write;
            app.OutputError += error.ThrowError;

            app.OnInit();
            app.OnStart();
        }
        void OnStart()
        {
            app.WriteMessage("+-------------------------------+\n");
            app.WriteMessage("| Welcome to Guess number game !|\n");
            app.WriteMessage("+-------------------------------+\n");
        }
        void OnFinish(string message)
        {
            app.WriteMessage("-------------------------\n");
            app.WriteMessage($"    {message} \n"); 
            app.WriteMessage("-------------------------\n"); 
            app.WriteMessage("Please press any key...");
            _ = app.ReadData();

            app.OutputError -= error.ThrowError;
            app.OutputData -= writer.Write;
            app.InputData -= reader.Read;

            Environment.Exit(0); 
        }
    }
}
