﻿using System;
using System.Linq;

namespace Reqex
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Regular expression");
            string url1 = @"https://docs.unity3d.com/Manual/index.html";
            string url2 = @"https://www.w3schools.com/tags/tag_img.asp";
            var html = new HTMLReader().Read(url2);
            var imagesUrl = new HtmlParser().GetImagesUrls(html);
            Console.WriteLine("Images url: ");
            imagesUrl.ToList().ForEach(x => Console.WriteLine(x));
            Console.ReadKey();
        }
    }
}
