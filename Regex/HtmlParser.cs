﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace Reqex
{
    internal class HtmlParser
    {
        private static Regex images = new Regex(@"<img.+?src ?= ?[""'']?(?<url>.+?)[""'' >]");
        public IEnumerable<string> GetImagesUrls(string html)
        {
            var r = images.Matches(html);
            return images.Matches(html).Select(x => x.Groups["url"].Value);
        }
    }
}
