﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Reqex
{
    internal class HTMLReader
    {
        public string Read(string url)
        {
            using (var client = new WebClient())
            {
                
                return client.DownloadString(url);
            }
        }
    }
}
