﻿using System;
using Cloneable.Lib;

namespace Cloneable.CUI
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=========  Cloneable UIElements ===========");

            Console.WriteLine(" - Create some UI elements");
            var text1 = new TextField 
            {
                Top = 10,
                Left = 10,
                Width = 57,
                Height = 15,
                Text = "Text Field N1"
            };
            var textBorder1 = new TextFieldBorder
            { 
                Top = 10,
                Left = 100,
                Width = 57,
                Height = 15,
                Text = "Bordered text field N1",
                BorderThickness =1,
                BorderColor = "Yellow"
            };

            var button1 = new Button
            {
                Top = 25,
                Left = 10,
                Width = 57,
                Height = 15,
                Text = "Button N1"
            };

            var buttonImg1 = new ButtonImg
            {
                Top = 25,
                Left = 10,
                Width = 57,
                Height = 15,
                Text = "Button N2",
                ImgPath = @"C:\projects\Smile.png"
            };

            Console.WriteLine(text1);
            Console.WriteLine(textBorder1);
            Console.WriteLine(button1);
            Console.WriteLine(buttonImg1);

            Console.WriteLine(" - Create clones");

            var text2 = text1.Clone();
            var textBorder2 = textBorder1.Clone();
            var button2 = button1.Clone();
            var buttonImg2 = buttonImg1.Clone();

            Console.WriteLine(text2);
            Console.WriteLine(textBorder2);
            Console.WriteLine(button2);
            Console.WriteLine(buttonImg2);

            Console.ReadLine();
            Console.WriteLine("==========   The end =============");
        }
    }
}
