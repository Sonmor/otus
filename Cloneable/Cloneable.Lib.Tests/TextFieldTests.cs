using NUnit.Framework;

namespace Cloneable.Lib.Tests
{
    [TestFixture]
    public class TextFieldTests
    {
        private TextField Create(int Top = 10, int Left = 10, int Width = 100, int Height = 20, string Text = "SomeText")
            => new TextField() 
            {
                Top = Top,
                Left = Left,
                Width = Width,
                Height = Height,
                Text = Text
            };

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(),Is.Not.Null);
        [Test] public void Top_AreEqualSendedValue_True() => Assert.That(Create(Top: 15).Top, Is.EqualTo(15));
        [Test] public void Left_AreEqualSendedValue_True() => Assert.That(Create(Left: 15).Left, Is.EqualTo(15));
        [Test] public void Width_AreEqualSendedValue_True() => Assert.That(Create(Width: 150).Width, Is.EqualTo(150));
        [Test] public void Height_AreEqualSendedValue_True() => Assert.That(Create(Height: 15).Height, Is.EqualTo(15));
        [Test] public void Text_AreEqualSendedValue_True() => Assert.That(Create(Text: "This is text").Text, Is.EqualTo("This is text"));
        [Test]
        public void Clone_NewObjectAreNotEqualSource_True()
        {
            var source = Create();
            var clone = source.Clone();
            Assert.That(clone, Is.Not.EqualTo(source));
        }
        [Test]
        public void Clone_NewGUIDAreNotEqualSource_True()
        {
            var source = Create();
            var clone = source.Clone();
            Assert.That(clone.Id, Is.Not.EqualTo(source.Id));
        }
        [Test]
        public void Clone_PropertiesAreEqualSource_True() => Assert.Multiple(() =>
        {
            var top = 15;
            var left = 100;
            var width = 48;
            var height = 23;
            var text = "Some text in text field";

            var expected = Create(Top: top, Left: left, Width: width, Height: height, Text:text).Clone();
            Assert.That(expected.Top, Is.EqualTo(top));
            Assert.That(expected.Left, Is.EqualTo(left));
            Assert.That(expected.Width, Is.EqualTo(width));
            Assert.That(expected.Height, Is.EqualTo(height));
            Assert.That(expected.Text, Is.EqualTo(text));
        });
        [Test]
        public void ToString_AreEqualExpected_True()
        {
            var top = 15;
            var left = 100;
            var width = 48;
            var height = 23;
            var text = "Some text in text field";
            var obj = Create(Top: top, Left: left, Width: width, Height: height, Text: text);
            var expected = $"TextField: Id: {obj.Id}; Top: {top}; Left: {left}; Width: {width}; Height: {height}; Text: {text}";

            Assert.That(obj.ToString(), Is.EqualTo(expected));
        }
    }
}