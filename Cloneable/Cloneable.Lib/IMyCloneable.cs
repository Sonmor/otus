﻿namespace Cloneable.Lib
{
    public interface IMyCloneable<T>
    {
        T Clone();
    }
}
