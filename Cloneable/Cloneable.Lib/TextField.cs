﻿namespace Cloneable.Lib
{
    public class TextField : UIElement
    {
        public string Text { get; set; }
        public TextField() : base() { }
        public TextField(TextField source):base(source)
        {
            this.Text = source.Text;
        }
        public override TextField Clone() => new(this);
        public override string ToString()
            => $"{base.ToString()}; Text: {Text}";
    }
}
