﻿namespace Cloneable.Lib
{
    public class TextFieldBorder:TextField
    {
        public int BorderThickness { get; set; }
        public string BorderColor { get; set; }
        public TextFieldBorder() : base() { }
        public TextFieldBorder(TextFieldBorder source) : base(source)
        {
            this.BorderThickness = source.BorderThickness;
            this.BorderColor = source.BorderColor;
        }

        public override TextFieldBorder Clone() => new(this);
        public override string ToString()
           => $"{base.ToString()}; BorderThickness: {BorderThickness}; BorderColor: {BorderColor}";
    }
}
