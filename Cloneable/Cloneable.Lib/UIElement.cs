﻿using System;
using System.Text;

namespace Cloneable.Lib
{
    public abstract class UIElement : IMyCloneable<UIElement>
    {
        public Guid Id { get; }
        protected readonly string TypeName;
        public int Height { get; set; }
        public int Width { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        protected UIElement() 
        {
            Id = Guid.NewGuid();
            TypeName = this.GetType().Name;
        }
        protected UIElement(UIElement source):this()
        {
            this.Top = source.Top;
            this.Left = source.Left;
            this.Width = source.Width;
            this.Height = source.Height;
        }
        public abstract UIElement Clone();
        
        public override string ToString()
            => $"{TypeName}: Id: {Id}; Top: {Top}; Left: {Left}; Width: {Width}; Height: {Height}";
    }
}
