﻿namespace Cloneable.Lib
{
    public class ButtonImg:Button
    {
        public string ImgPath { get; set; }
        public ButtonImg() : base() { }
        public ButtonImg(ButtonImg source) : base(source)
        {
            this.ImgPath = source.ImgPath;
        }
        public override ButtonImg Clone() => new(this);
        public override string ToString()
            => $"{base.ToString()}; ImgPath: {ImgPath}";
    }
}
