﻿namespace Cloneable.Lib
{
    public class Button:UIElement
    {
        public string Text { get; set; }
        public Button() : base() { }
        public Button(Button source) : base(source)
        {
            this.Text = source.Text;
        }
        public override Button Clone() => new(this);
        public override string ToString()
            => $"{base.ToString()}; Text: {Text}";
    }
}
