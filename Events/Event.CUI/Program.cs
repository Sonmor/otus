﻿using System;
using Event.Inf;

namespace Event.CUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new(new EventApp());
            prog.Start();

            Console.WriteLine("  Game Over ");
            Console.Write("Please enter any key for exit ... ");
            _ = Console.ReadLine();
            
        }
        private EventApp app;
        internal Program(EventApp app)
        {
            this.app = app;
            app.AddHandlerToDocumentReadyEvent(OnDocumentReady);
            app.AddHandlerToTimeoutEvent(OnTimeOut);
        }

        internal void Start()
        {
            Console.Write("Please insert folder path: ");
            string folder = Console.ReadLine();
            Console.Write("Please insert timeout (ms): ");
            int.TryParse(Console.ReadLine(), out int timeout);
            app.Start(folder, timeout);
        }
        internal void OnDocumentReady()
        {
            Console.WriteLine("Congratulation !!! Your documents are ready!!!!");
        }
        internal void OnTimeOut()
        {
            Console.WriteLine("I'm sorry!! Time is over");
        }
    }
}
