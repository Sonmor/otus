using NUnit.Framework;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Events.Domain.Tests
{
    public class DocumentsRecieverTests
    {
        private DocumentsReceiver Create(IFilesReceiver files = null, IEnumerable<string> ObserverFiles = null)
        {
            files ??= new Mock<IFilesReceiver>().Object;
            ObserverFiles ??= Enumerable.Empty<string>();

            return new DocumentsReceiver(FileReceiver: files, ObserverFiles: ObserverFiles);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void DocumentReady_WhenAllDocumentsAreExist_RaiseEvent()
        {
            IEnumerable<string> observer = new List<string> { "file1", "file2", "file3" };
            var files = new Mock<IFilesReceiver>();
            files.Setup(x => x.GetFiles(It.IsAny<string>())).Returns(observer);

            var receiver = Create(files: files.Object, ObserverFiles: observer);
            bool ready = false;
            bool timeout = false;
            receiver.DocumentsReady += () => ready = true;
            receiver.TimedOut += () => timeout = true;
            receiver.Start("temp",2000);
            Assert.Multiple(() =>
            {
                Assert.That(ready, Is.True);
                Assert.That(timeout, Is.False);
            });
        }
        [Test]
        public void DocumentReady_WhenNotAllDocumentsAreExist_NotRaiseEvent()
        {
            IEnumerable<string> observer = new List<string> { "file1", "file2", "file3" };
            var files = new Mock<IFilesReceiver>();
            files.Setup(x => x.GetFiles(It.IsAny<string>())).Returns(new List<string> { "file1", "file2"});
            var receiver = Create(files: files.Object, ObserverFiles: observer);
            bool ready = false;
            bool timeout = false;
            receiver.DocumentsReady += () => ready = true;
            receiver.TimedOut += () => timeout = true;
            receiver.Start("temp",2000);
            Assert.Multiple(() =>
            {
                Assert.That(ready, Is.False);
                Assert.That(timeout, Is.True);
            });
        }
    }
}