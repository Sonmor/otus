﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Events.Domain.Tests
{
    [TestFixture]
    public class FileReceiverTests
    {
        FileReceiver Create() => new();

        [Test] public void CTOR_IsNotNullByDefault_True() => Assert.That(Create(), Is.Not.Null);
        [Test]
        public void GetFiles_CheckDiskFolder_True()
        {
            var receiver = Create();
            var files = receiver.GetFiles(@"D:\temp\");
            Assert.That(files.Count(),Is.EqualTo(3));
        }
      
    }
}
