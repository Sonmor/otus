﻿using System.Collections.Generic;

namespace Events.Domain
{
    public interface IFilesReceiver
    {
        IEnumerable<string> GetFiles(string FolderName);
    }
}
