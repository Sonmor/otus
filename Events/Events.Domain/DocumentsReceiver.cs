﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Events.Domain
{
    public class DocumentsReceiver
    {
        public event Action DocumentsReady;
        public event Action TimedOut;

        public readonly IFilesReceiver FileReceiver;
        internal readonly IEnumerable<string> ObserverFiles;

        private bool done;

        public DocumentsReceiver(IFilesReceiver FileReceiver, IEnumerable<string> ObserverFiles)
        {
            this.FileReceiver = FileReceiver;
            this.ObserverFiles = ObserverFiles;
        }

        public void Start(string FolderName, int waitingInterval)
        {
            done = false;
            Timer timer = new(CheckFolder,FolderName, 200, 200);
            Thread.Sleep(waitingInterval);
            timer.Dispose();
            if(!done && TimedOut != null)
                TimedOut.Invoke();
        }
        internal void CheckFolder(object state )
        {
            string folder = state as string;
            var allfiles = FileReceiver.GetFiles(folder).ToList();
            int count = 0;
            foreach (var file in ObserverFiles)
                if (allfiles.Contains(file)) count++;

            if (!done && count == ObserverFiles.Count() && DocumentsReady != null )
            {
                done = true;
                DocumentsReady.Invoke();
            }
        }
    }
} 