﻿using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Events.Domain
{
    public class FileReceiver : IFilesReceiver
    {
        public IEnumerable<string> GetFiles(string FolderName)
            => Directory.Exists(FolderName) ? Directory.GetFiles(FolderName).Select(x => Path.GetFileName(x)) : Enumerable.Empty<string>();
    }
}
