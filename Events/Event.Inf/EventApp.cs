﻿using System;
using System.Collections.Generic;
using Events.Domain;

namespace Event.Inf
{
    public class EventApp
    {
        private readonly DocumentsReceiver receiver;
        public EventApp()
        {
            List<string> files = new()
            {
                "Паспорт.jpg",
                "Заявление.txt",
                "Фото.jpg"
            };
            receiver = new DocumentsReceiver(new FileReceiver(), files);
        }

        public void Start(string FolderName, int timeOutInterval) => receiver.Start(FolderName,timeOutInterval);
        public void AddHandlerToDocumentReadyEvent(Action handler) => receiver.DocumentsReady += handler;
        public void AddHandlerToTimeoutEvent(Action handler) => receiver.TimedOut += handler;

    }
}
