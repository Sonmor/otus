﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Concurrency.Import.WebAPI.Controllers
{
    /// <summary>
    /// Controller of customers
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ICustomerRepository repo;

        /// <summary>
        /// CTOR has ICustomerRepository parameter 
        /// </summary>
        /// <param name="repo"></param>
        public UsersController(ICustomerRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Get list of all customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Customer>> Get()
        {
            var result = await repo.GetAllCustomersAsync();
            return result; 
        }

        /// <summary>
        /// Get users/{id}
        /// </summary>
        /// <param name="id">Customer id</param>
        /// <returns>Retriev customer from DB</returns>
        [HttpGet("id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> GetById(int id)
        {
            var customer = await repo.FindCustomerAsync(id);
            return customer == null ? NotFound() : Ok(customer);
        }

        /// <summary>
        /// Post: users
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult> Post([FromBody] Customer customer)
        {
            await this.repo.AddCustomerAsync(customer);
            return CreatedAtAction(nameof(GetById), new { id = customer.Id}, customer);
        }
    }
}
