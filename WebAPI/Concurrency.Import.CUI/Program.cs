﻿using System;
using System.Linq;
using Concurrency.Import.CUI.Inf;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Concurrency.Import.CUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new(new ClientApp(writer: new ConsoleWriter(), reader:new ConsoleReader(), error: new ConsoleError()));
            prog.OnStart();
        }
        private readonly IClientApp app;
        private readonly ConsoleMenu menu;

        protected Program(IClientApp app)
        {
            this.app = app;
            this.menu = new();
            menu.MenuItems.Add(new Tuple<int, string, Action>(0, "Exit", Finish));
            menu.MenuItems.Add(new Tuple<int, string, Action>(1, "Add a User", OnAddNewUser));
            menu.MenuItems.Add(new Tuple<int, string, Action>(2, "Show list of Users", OnShowAllUsers));
            menu.MenuItems.Add(new Tuple<int, string, Action>(3, "Show User info", OnShowUser));

            app.Exit += Finish;
        }
        internal void OnShowAllUsers()
        {
            app.OnWrite("+-------------------------+\n");
            app.OnWrite("|    Show All Users       |\n");
            app.OnWrite("+-------------------------+\n");
            var users = app.GetAllUsers();
            foreach(var user in users.Result)
                app.OnWrite($"{user}\n");

        }
        internal void OnShowUser()
        {
            app.OnWrite("+-------------------------+\n");
            app.OnWrite("|    Show User info       |\n");
            app.OnWrite("+-------------------------+\n");
            app.OnWrite("Please provide user id: ");

            int id = -1;
            
            while (!int.TryParse(app.OnRead(), out id))
            {
                app.OnThrowError("Incorrect value, please enter id again");
            }
            var user = app.GetUser(id).Result;
            if(user != null)
                app.OnWrite($"{user}\n");
        }
        internal void OnAddNewUser()
        {
            app.OnWrite("+-------------------------+\n");
            app.OnWrite("|    Add new User         |\n");
            app.OnWrite("+-------------------------+\n");
            app.OnWrite("Please provide some information about new User: \n");

            try
            {
                Customer user = CreateUser();
                app.OnAddNewUser(user);
            }
            catch (Exception ex)
            {
                app.OnThrowError(ex.Message);
            }
        }

        internal Customer CreateUser()
        {
            app.OnWrite("Please insert User's Full Name: ");
            string FullName = app.OnRead();
            app.OnWrite("Please insert User's email: ");
            string Email = app.OnRead();
            app.OnWrite("Please insert User's Phone: ");
            string Phone = app.OnRead();

            return new Customer()
            {
                FullName = FullName,
                Email = Email,
                Phone = Phone
            };
        }


        public void OnStart()
        {
            app.OnWrite("+------------------------------+\n");
            app.OnWrite("|    Welcome to WebAPI Client  |\n");
            app.OnWrite("+------------------------------+\n");
            app.OnWrite("Please press any key ... ");
            _ = app.OnRead();
            do
            {
                app.OnClear();
                _ = menu.DisplayMenu();
                if (!int.TryParse(app.OnRead(), out int num))
                {
                    app.OnThrowError("Incorrect value, please send again ..\n");
                    continue;
                }
                try
                {
                    menu.MenuItems.First(x => x.Item1 == num).Item3();
                }
                catch
                {
                    app.OnThrowError($"Our menu havn't numer {num}. Please send another number");
                }
                app.OnWrite("Please press enter to continue... ");
                _ = app.OnRead();

            } while (true);
        }

        internal void Finish()
        {
            app.OnWrite("+------------------------------+\n");
            app.OnWrite("| WebAPI Client app is closed  |\n");
            app.OnWrite("+------------------------------+\n");
            app.OnWrite("Please press any key to exit ...");
            _ = app.OnRead();

            Environment.Exit(0);
        }
    }
}
