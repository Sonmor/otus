﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Concurrency.Import.CUI
{
    public interface IClientApp
    {
        event Action Exit;

        void OnExit();
        string OnRead();
        void OnThrowError(string message);
        void OnWrite(string message);
        Task<IEnumerable<Customer>> GetAllUsers();
        Task<Customer> GetUser(int id);
        Task OnAddNewUser(Customer customer);
        void OnClear();
    }
}