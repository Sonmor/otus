﻿using Concurrency.Import.CUI.Inf;
using Otus.Teaching.Concurrency.Import.Core.IO;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Linq;
using System.Text;

namespace Concurrency.Import.CUI
{
    public class ClientApp : IClientApp
    {
        private readonly string _api = @"http://localhost:5001/api/users";
        private readonly IWriterUI writer;
        private readonly IReaderUI reader;
        private readonly IErrorUI error;

        public event Action Exit;
        public event Action Start;

        public ClientApp(IWriterUI writer, IReaderUI reader, IErrorUI error)
        {
            this.writer = writer;
            this.reader = reader;
            this.error = error;
        }

        public void OnWrite(string message) => writer.Write(message);
        public string OnRead() => reader.Read();
        public void OnThrowError(string message) => error.ThrowError(message);
      
        public void OnExit() => Exit.Invoke();

        public async Task<IEnumerable<Customer>> GetAllUsers()
        {
            IEnumerable<Customer> result;
            using (HttpClient client = new())
            {
                try
                {
                    var response = await client.GetAsync(_api);
                    if (response.IsSuccessStatusCode)
                    {
                        var t = await response.Content.ReadAsStringAsync();
                        result = JsonSerializer.Deserialize<IEnumerable<Customer>>(t, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true});
                    }
                    else
                    {
                        result = Enumerable.Empty<Customer>();
                    }
                }
                catch(Exception ex)
                {
                    OnThrowError(ex.Message);
                    result = Enumerable.Empty<Customer>();
                }
            }
            return result;
        }

        public async Task<Customer> GetUser(int id)
        {
            Customer user = null;
            using(HttpClient client = new())
            {
                try
                {
                    // /Users/id?id=32
                    var response = await client.GetAsync($"{ _api}/id?id={id}");
                    if (response.IsSuccessStatusCode)
                    {
                        var context = await response.Content.ReadAsStringAsync(); 
                        user = JsonSerializer.Deserialize<Customer>(context, new JsonSerializerOptions { PropertyNameCaseInsensitive = true});
                    }
                    else
                    {
                        OnThrowError($"Can't find user with id = {id}");
                    }
                }
                catch (Exception ex)
                {
                    OnThrowError(ex.Message);
                }
                return user;
            }
        }

        public async Task OnAddNewUser(Customer customer)
        {
            var user = JsonSerializer.Serialize<Customer>(customer);
            var content = new StringContent(user,Encoding.UTF8,"application/json");
            using (HttpClient client = new())
            {
                var response = await client.PostAsync(_api, content);
                if (response.IsSuccessStatusCode)
                {
                    OnWrite($"\nUser: {customer} was saved");
                }
                else
                {
                    OnThrowError($"Cannot save user: {response.ReasonPhrase}");
                }
            }
        }

        public void OnClear() => reader.Clear();
    }
}