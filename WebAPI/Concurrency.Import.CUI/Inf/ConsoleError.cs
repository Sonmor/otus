﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Concurrency.Import.CUI.Inf
{
    public class ConsoleError : IErrorUI
    {
        public void ThrowError(string error)
            => Console.WriteLine($"Error: {error.Replace("\n", Environment.NewLine)}");
    }
}
