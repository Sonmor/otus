﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Concurrency.Import.CUI.Inf
{
    public class ConsoleReader : IReaderUI
    {
        public string Read() => Console.ReadLine();
        public void Clear() => Console.Clear();
    }
}
