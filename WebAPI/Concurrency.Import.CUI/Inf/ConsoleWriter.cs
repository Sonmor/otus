﻿using Otus.Teaching.Concurrency.Import.Core.IO;
using System;
namespace Concurrency.Import.CUI.Inf
{
    public class ConsoleWriter : IWriterUI
    {
        public void Write(string message)
            => Console.Write(message.Replace("\n", Environment.NewLine));
    }
}
