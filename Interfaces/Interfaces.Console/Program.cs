﻿using Interfaces.Lib;
using Interfaces.Lib.Comparers;

using System.Linq;
using System.Collections.Generic;

namespace Interfaces.Console
{
    class Program
    {
        protected Program() { }

        static void Main(string[] args)
        {
            System.Console.WriteLine("Interfaces");
            Part1();
            Part2();
            Part3();

            System.Console.ReadKey();
        }

        private static void Part1()
        {
            PersonCollection family;
            System.Console.WriteLine("\nPart 1:\n");
            using (System.IO.StreamReader stream = new System.IO.StreamReader("Simpsons.xml"))
            {
                var serializer = new XmlSerializer<PersonCollection>();
                family = serializer.Deserialize(stream.BaseStream);
            }

            PrintPersonCollection(family);

            System.Console.WriteLine("\n--------------------------------------------------------------------\n");
            System.Console.WriteLine($"PersonCollection is IEnumerable<Person> : {family is IEnumerable<Person>}");

            System.Console.WriteLine($"Simpson family contains person with Homer first name: {family.Any(s => "Homer" == s.FirstName)}");
            var adult = family.Where(s => s.Age >= 18);
            System.Console.WriteLine("\nAdult:");
            PrintPersonCollection(adult);

            var arr = new PersonXmlCollection("SimpsonArray.xml");
            System.Console.WriteLine("\n--------------------------------------------------------------------\n");
            System.Console.WriteLine($"PersonXmlCollection is IEnumerable<Person> : {arr is IEnumerable<Person>}");

            PrintPersonCollection(arr);
        }

        private static void Part2()
        {
            System.Console.WriteLine("\nPart 2:\n");

            PersonCollection family;
            using (System.IO.StreamReader stream = new System.IO.StreamReader("Simpsons.xml"))
            {
                var serializer = new XmlSerializer<PersonCollection>();
                family = serializer.Deserialize(stream.BaseStream);
            }

            System.Console.WriteLine($"PersonCollection is IAlgorithm : {family is IAlgorithm}");
            System.Console.WriteLine("\n-------------------  Sorting  ----------------------------------------------\n");
            System.Console.WriteLine("\nSorted collection by First Name");
            family.Sort(new PersonFirstNameComparer());
            PrintPersonCollection(family);

            System.Console.WriteLine("\nSorted collection by Age");
            family.Sort(new PersonAgeComparer());
            PrintPersonCollection(family);
        }
        private static void Part3()
        {
            System.Console.WriteLine("\nPart 3:\n");

            IRepository<Account> repository = new ConcreetRepository();

            AccountService service = new AccountService(repository);
            service.AddAccount(new Account() 
            { 
                FirstName = "Homer",
                LastName = "Simpson",
                BirthDate = new System.DateTime(1978, 7,18)
            });

            service.AddAccount(new Account() 
            {
                FirstName = "Marge",
                LastName = "Simpson",
                BirthDate = new System.DateTime(1982,5,14)
            });

            service.AddAccount(new Account()
            {
                FirstName = "Bart",
                LastName = "Simpson",
                BirthDate = new System.DateTime(2009, 2,15)
            });

            service.AddAccount(new Account()
            {
                FirstName ="Lisa",
                LastName = "Simpson",
                BirthDate = new System.DateTime(2012, 3, 5)
            });

            service.AddAccount(new Account() 
            {
                FirstName = "Maggie",
                LastName = "Simpson",
                BirthDate = new System.DateTime(2020, 8, 27)
            });

            service.AddAccount(new Account()
            {
                FirstName = string.Empty,
                LastName = string.Empty,
                BirthDate = new System.DateTime(2015, 6,3)
            });

            var accounts = repository.GetAll();

            foreach (var account in accounts)
                System.Console.WriteLine($"First Name: {account.FirstName}; Last Name: {account.LastName}; Age: {System.DateTime.Now.Year - account.BirthDate.Year}");

        }
        public static void PrintPersonCollection(IEnumerable<Person> people)
        {
            foreach (var p in people)
                System.Console.WriteLine(p);
        }
    }
}
