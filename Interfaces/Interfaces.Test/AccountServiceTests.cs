﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using FakeItEasy;

namespace Interfaces.Lib.Test
{
    public class AccountServiceTests
    {
        private AccountService Create(IRepository<Account> repository = null)
        {
            repository = repository ?? A.Fake<IRepository<Account>>();
            return new AccountService(repository);
        }

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.IsNotNull(Create());
        [Test]
        public void AddAccount_AddToRepositoryCorrectAccount_True()
        {
            var account = new Account()
            {
                FirstName = "Homer",
                LastName = "Simpson",
                BirthDate = new DateTime(1978, 7, 18)
            };
            var repository = A.Fake<IRepository<Account>>();
            var service = Create(repository);
            service.AddAccount(account);

            A.CallTo(() => repository.Add(account)).MustHaveHappened();
        }
        [Test]
        public void AddAccount_DontSaveAccountWithEmptyFirstName_True()
        {
            var account = new Account()
            {
                FirstName = String.Empty,
                LastName = "Simpson",
                BirthDate = new DateTime(1978, 7, 18)
            };
            var repository = A.Fake<IRepository<Account>>();
            var service = Create(repository);
            service.AddAccount(account);
            A.CallTo(() => repository.Add(account)).MustNotHaveHappened();
        }

        [Test]
        public void AddAccount_DontSaveAccountWithEmptyLastName_True()
        {
            var account = new Account()
            {
                FirstName = "Homer",
                LastName = String.Empty,
                BirthDate = new DateTime(1978, 7, 18)
            };
            var repository = A.Fake<IRepository<Account>>();
            var service = Create(repository);
            service.AddAccount(account);
            A.CallTo(() => repository.Add(account)).MustNotHaveHappened();
        }
        [Test]
        public void AddAccount_DontSaveAccountWithAgeLess18_True()
        {
            var account = new Account()
            {
                FirstName = "Bart",
                LastName = "Simpson",
                BirthDate = new DateTime(2009, 2, 14)
            };
            var repository = A.Fake<IRepository<Account>>();
            var service = Create(repository);
            service.AddAccount(account);
            A.CallTo(() => repository.Add(account)).MustNotHaveHappened();
        }
    }
}
