using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using NUnit.Framework;

namespace Interfaces.Lib.Test
{
    [TestFixture]
    public class XmlSerializerTests
    {
        private XmlSerializer<T> Create<T>() => new XmlSerializer<T>();

        [Test] public void CTOR_CreatedObjectIsNotNullByDefault_True() => Assert.IsNotNull(Create<Person>());
        [Test]
        public void Serialize_ReturnStringAreEqualExpected_True()
        {
            string expected = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" />";
            Person Homer = new Person() 
            {
                FirstName = "Homer",
                LastName = "Simpson",
                Age = 42
            };
            var serializer = Create<Person>();
            var actual = serializer.Serialize(Homer);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Deserializer_ReturnedObjectAreEqualExpected_True()
        { 
            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" />";
            byte[] byteArray = Encoding.UTF8.GetBytes(xml);

            var stream = new MemoryStream(byteArray);
            var serializer = Create<Person>();
            var Homer = serializer.Deserialize(stream);
            Assert.Multiple(() => 
            {
                Assert.AreEqual("Homer", Homer.FirstName);
                Assert.AreEqual("Simpson", Homer.LastName);
                Assert.AreEqual(42,Homer.Age);
            });
        }
        [Test]
        public void Deserializer_RetrunCollectionAreEqualExpected_True()
        {

            var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<PersonCollection xmlns:sys=\"https://extendedxmlserializer.github.io/system\" xmlns:exs=\"https://extendedxmlserializer.github.io/v2\">\r\n  <Items Capacity=\"8\">\r\n    <Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Marge\" LastName=\"Simpson\" Age=\"39\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Bart\" LastName=\"Simpson\" Age=\"12\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Lisa\" LastName=\"Simpson\" Age=\"9\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Maggie\" LastName=\"Simpson\" Age=\"1\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n  </Items>\r\n</PersonCollection>";
            byte[] byteArray = Encoding.UTF8.GetBytes(xml);

            var stream = new MemoryStream(byteArray);
            var serializer = Create<PersonCollection>();
            var family = serializer.Deserialize(stream);
            Assert.Multiple(() =>
            {
                Assert.AreEqual("Homer",family.Items[0].FirstName);
                Assert.AreEqual("Simpson", family.Items[0].LastName);
                Assert.AreEqual(42,family.Items[0].Age);

                Assert.AreEqual("Marge", family.Items[1].FirstName);
                Assert.AreEqual("Simpson", family.Items[1].LastName);
                Assert.AreEqual(39, family.Items[1].Age);

                Assert.AreEqual("Bart", family.Items[2].FirstName);
                Assert.AreEqual("Simpson", family.Items[2].LastName);
                Assert.AreEqual(12,family.Items[2].Age);

                Assert.AreEqual("Lisa", family.Items[3].FirstName);
                Assert.AreEqual("Simpson", family.Items[3].LastName);
                Assert.AreEqual(9, family.Items[3].Age);

                Assert.AreEqual("Maggie",family.Items[4].FirstName);
                Assert.AreEqual("Simpson", family.Items[4].LastName);
                Assert.AreEqual(1,family.Items[4].Age);
            });

        }
        [Test]
        public void Serialize_ReturnedXMLAreEqualExpected_True()
        {
            var expected = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<PersonCollection xmlns:sys=\"https://extendedxmlserializer.github.io/system\" xmlns:exs=\"https://extendedxmlserializer.github.io/v2\">\r\n  <Items Capacity=\"8\">\r\n    <Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Marge\" LastName=\"Simpson\" Age=\"39\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Bart\" LastName=\"Simpson\" Age=\"12\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Lisa\" LastName=\"Simpson\" Age=\"9\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n    <Person FirstName=\"Maggie\" LastName=\"Simpson\" Age=\"1\" xmlns=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" />\r\n  </Items>\r\n</PersonCollection>";
            PersonCollection family = new PersonCollection();
            family.Items = new List<Person>()
            {
                new Person()
                {
                    FirstName = "Homer",
                    LastName = "Simpson",
                    Age =42
                },
                new Person()
                {
                    FirstName = "Marge",
                    LastName = "Simpson",
                    Age =39
                },
                new Person()
                {
                    FirstName = "Bart",
                    LastName = "Simpson",
                    Age=12
                },
                new Person()
                {
                    FirstName = "Lisa",
                    LastName = "Simpson",
                    Age = 9
                },
                new Person()
                {
                    FirstName = "Maggie",
                    LastName = "Simpson",
                    Age =1
                }
            };

            var serializer = Create<PersonCollection>();
            var actual = serializer.Serialize(family);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Serializer_ReturnedStringAreEqualExpected_True()
        {
            string expected = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Array xmlns:exs=\"https://extendedxmlserializer.github.io/v2\" xmlns:ns1=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" exs:item=\"ns1:Person\" xmlns=\"https://extendedxmlserializer.github.io/system\">\r\n  <ns1:Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" />\r\n  <ns1:Person FirstName=\"Marge\" LastName=\"Simpson\" Age=\"39\" />\r\n  <ns1:Person FirstName=\"Bart\" LastName=\"Simpson\" Age=\"12\" />\r\n  <ns1:Person FirstName=\"Lisa\" LastName=\"Simpson\" Age=\"9\" />\r\n  <ns1:Person FirstName=\"Maggie\" LastName=\"Simpson\" Age=\"1\" />\r\n</Array>";
           Person[] people = new Person[] 
            {
                new Person()
                {
                    FirstName = "Homer",
                    LastName = "Simpson",
                    Age =42
                },
                new Person()
                {
                    FirstName = "Marge",
                    LastName = "Simpson",
                    Age =39
                },
                new Person()
                {
                    FirstName = "Bart",
                    LastName = "Simpson",
                    Age=12
                },
                new Person()
                {
                    FirstName = "Lisa",
                    LastName = "Simpson",
                    Age = 9
                },
                new Person()
                {
                    FirstName = "Maggie",
                    LastName = "Simpson",
                    Age =1
                }
            };

            var serializer = Create<Person[]>();
            var actual = serializer.Serialize(people);
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void Deserializer_ReturnPersonArrayAreEqualExpected_True()
        {
            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<Array xmlns:exs=\"https://extendedxmlserializer.github.io/v2\" xmlns:ns1=\"clr-namespace:Interfaces.Lib;assembly=Interfaces.Lib\" exs:item=\"ns1:Person\" xmlns=\"https://extendedxmlserializer.github.io/system\">\r\n  <ns1:Person FirstName=\"Homer\" LastName=\"Simpson\" Age=\"42\" />\r\n  <ns1:Person FirstName=\"Marge\" LastName=\"Simpson\" Age=\"39\" />\r\n  <ns1:Person FirstName=\"Bart\" LastName=\"Simpson\" Age=\"12\" />\r\n  <ns1:Person FirstName=\"Lisa\" LastName=\"Simpson\" Age=\"9\" />\r\n  <ns1:Person FirstName=\"Maggie\" LastName=\"Simpson\" Age=\"1\" />\r\n</Array>";
            byte[] byteArray = Encoding.UTF8.GetBytes(xml);

            var stream = new MemoryStream(byteArray);
            var serializer = Create<Person[]>();
            var family = serializer.Deserialize(stream);
             
            Assert.Multiple(() =>
            {
                Assert.AreEqual("Homer", family[0].FirstName);
                Assert.AreEqual("Simpson", family[0].LastName);
                Assert.AreEqual(42, family[0].Age);

                Assert.AreEqual("Marge", family[1].FirstName);
                Assert.AreEqual("Simpson", family[1].LastName);
                Assert.AreEqual(39, family[1].Age);

                Assert.AreEqual("Bart", family[2].FirstName);
                Assert.AreEqual("Simpson", family[2].LastName);
                Assert.AreEqual(12, family[2].Age);

                Assert.AreEqual("Lisa", family[3].FirstName);
                Assert.AreEqual("Simpson", family[3].LastName);
                Assert.AreEqual(9, family[3].Age);

                Assert.AreEqual("Maggie", family[4].FirstName);
                Assert.AreEqual("Simpson", family[4].LastName);
                Assert.AreEqual(1, family[4].Age);
            });


        }
    }
}