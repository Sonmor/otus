﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Interfaces.Lib
{
    public sealed class PersonXmlCollection : IEnumerable<Person>
    {
        readonly string file;
        public PersonXmlCollection(string XMLFile)
        {
            file = XMLFile;
        }
        public IEnumerator<Person> GetEnumerator()
        {
            if (!File.Exists(file)) throw new FileNotFoundException(file);

            using (StreamReader stream = new StreamReader(file))
            {
                var serializer = new XmlSerializer<Person[]>();

                var people = serializer.Deserialize(stream.BaseStream);
                int count = people.Length;
                for (int i = 0; i < count; i++)
                    yield return people[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
