﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Interfaces.Lib
{
    public class PersonCollection : IEnumerable<Person>, IAlgorithm
    {
        public List<Person> Items { get; set; } = new List<Person>();


        public IEnumerator<Person> GetEnumerator()
        {
            for (int i = 0; i < Items.Count; i++)
                yield return Items[i];
        }

        public void Sort(IComparer<Person> comparer) => Items.Sort(comparer);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

    }
}
