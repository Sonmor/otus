﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Interfaces.Lib.Comparers
{
    public class PersonFirstNameComparer : IComparer<Person>
    {
        public int Compare([AllowNull] Person x, [AllowNull] Person y) => x.FirstName.CompareTo(y.FirstName);
    }
}
