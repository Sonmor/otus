﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Interfaces.Lib.Comparers
{
    public class PersonAgeComparer : IComparer<Person>
    {
        public int Compare([AllowNull] Person x, [AllowNull] Person y) => x.Age.CompareTo(y.Age);
    }
}
