﻿using System;
using System.Collections.Generic;

namespace Interfaces.Lib
{
    public interface IAlgorithm
    {
        void Sort(IComparer<Person> comparer);
    }
}
