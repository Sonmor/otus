﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;



namespace Interfaces.Lib
{
    public class ConcreetRepository : IRepository<Account>
    {
        readonly XmlSerializer<List<Account>> accounts = new XmlSerializer<List<Account>>();

        readonly string defaultFileName = "accounts.xml";
        public void Add(Account item)
        {

            List<Account> collection;
            collection = ReadFile();

            collection.Add(item);
            using (StreamWriter writer = new StreamWriter(defaultFileName, false))
            {
                writer.Write(accounts.Serialize(collection));
            }
        }

        private List<Account> ReadFile()
        {
            List<Account> collection;
            if (File.Exists(defaultFileName))
            {
                using (var reader = new StreamReader(defaultFileName))
                {
                    collection = accounts.Deserialize(reader.BaseStream);
                }
            }
            else
                collection = new List<Account>();
            return collection;
        }

        public IEnumerable<Account> GetAll() => ReadFile().AsEnumerable();

        public Account GetOne(Func<Account, bool> predicate) =>ReadFile().FirstOrDefault(predicate);
    }
}
