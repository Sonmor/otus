using System.IO;

namespace Interfaces.Lib
{
    interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(Stream stream);
    }    
}