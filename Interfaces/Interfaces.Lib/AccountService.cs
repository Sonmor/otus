using System;

namespace Interfaces.Lib
{
   public class AccountService : IAccountService
    {
        private readonly IRepository<Account> repository;
        public AccountService(IRepository<Account> repository)
        {
            this.repository = repository;
        }
        public void AddAccount(Account account)
        {
            if(String.IsNullOrEmpty(account.FirstName) && String.IsNullOrEmpty(account.LastName) && (DateTime.Now.Year - account.BirthDate.Year) < 18)
            {
                throw new ArgumentException(account);
            }
            repository.Add(account);
        }
    }
}
