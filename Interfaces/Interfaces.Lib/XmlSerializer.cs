﻿using System.IO;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace Interfaces.Lib
{
    public class XmlSerializer<T>:ISerializer<T>
    {
        protected IExtendedXmlSerializer serializer = new ConfigurationContainer()
            .UseAutoFormatting()
            .UseOptimizedNamespaces()
            .EnableImplicitTyping(typeof(T))
            .Create();

        public string Serialize(T item) => serializer.Serialize(new XmlWriterSettings { Indent = true }, item);
        public T Deserialize(Stream stream) => serializer.Deserialize<T>(stream);
    }
}
