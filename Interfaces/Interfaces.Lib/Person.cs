﻿namespace Interfaces.Lib
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public override string ToString() => $"First Name: {FirstName}; Last Name: {LastName}; Age: {Age}";
    }
}
